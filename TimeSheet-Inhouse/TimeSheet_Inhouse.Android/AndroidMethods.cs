﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TimeSheet_Inhouse.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidMethods))]
namespace TimeSheet_Inhouse.Droid
{
    
   public class AndroidMethods : IAndroidMethods
    {

        private static Context context;

        public void CloseApp()
        {
            
                Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }
        

    }
}