﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Edit_Timesheet : ContentPage, IElementConfiguration<Frame>
    {
        public Label p_lab, t_lab1, dur_lab2, d_lab3, dateshow_lab, networkStatus;
        public Picker projectname, task;
        public Entry time_dur, description;
        public Button resubmit;
        DateTime date_change;
        string pick_project, dateformat, seconds_covert, pick_task, hrs_value, min_value, time, desc, setcompid;
        public Int32 BorderRadius { get; set; }
        public Edit_Timesheet(string projectname_edit, string taskname_edit, string id_edit, string hours_edit, string min_edit, string date_edit)
        {
            //InitializeComponent();
            this.Title = "Edit the TimeSheet";
            check_connection();

            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
            if (App.Current.Properties.ContainsKey("Dateformat"))
                dateformat = (string)App.Current.Properties["Dateformat"];
        
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
            date_change = Convert.ToDateTime(date_edit);

            p_lab = new Label
            {
                Text = "Project Name",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start

            };


            t_lab1 = new Label
            {
                Text = "Task Name",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start

            };
            dur_lab2 = new Label
            {
                Text = "Duration",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start

            };
            d_lab3 = new Label
            {
                Text = "Date:",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start

            };
            dateshow_lab = new Label
            {
                Text = date_change.ToString(dateformat),
                //Text = date_edit,

                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start

            };
            Picker hrs_picker = new Picker
            {
                Title = hours_edit,


                //VerticalOptions = LayoutOptions.Start
            };
            Picker min_picker = new Picker
            {
                Title = min_edit
                // VerticalOptions=LayoutOptions.End
            };
            Label spli_time = new Label
            {
                Text = ":",
                VerticalOptions = LayoutOptions.Center
            };
            List<string> hrs = new List<string> { };

            for (int i = 0; i < 16; i++)
            {
                if (i < 10)
                {
                    hrs_picker.Items.Add("0" + i.ToString());
                }
                else
                {
                    hrs_picker.Items.Add(i.ToString());

                }


            }
            List<string> mins = new List<string> { };

            for (int i = 0; i < 60; i = i + 15)
            {
                if (i < 10)
                {
                    min_picker.Items.Add("0" + i.ToString());
                }
                else
                {
                    min_picker.Items.Add(i.ToString());

                }


            }
            //  dateshow_lab.SetBinding(Label.TextProperty, DateTime.Today.ToString());
            projectname = new Picker
            {
                Title = projectname_edit,

                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            projectname.IsEnabled = false;
            LoadProject();
            projectname.Items.Add("Choose Project");
            // project name binding here from the url 
            //projectname.ItemDisplayBinding = new Binding(Constants.project_name);
            projectname.SelectedIndexChanged += (sender, args) => {
                string projectselected = projectname.Items[projectname.SelectedIndex];

                LoadTask(projectselected);

            };

            task = new Picker
            {
                Title = taskname_edit.ToString(),
                VerticalOptions = LayoutOptions.Start,
            };
            task.IsEnabled = false;
            task.ItemDisplayBinding = new Binding(Constants.task_name);
            time_dur = new Entry
            {
                Text = hours_edit,
                Placeholder = hours_edit,
                VerticalOptions = LayoutOptions.Start,
                Keyboard = Keyboard.Numeric,


            };
            description = new Entry
            {
                Placeholder = "Task description"
            };
            resubmit = new Button
            {
                Text = "Resubmit for Approval",
                BackgroundColor = Color.FromHex("#0077b9"),
                TextColor = Color.White,

                BorderRadius = 20,
                HorizontalOptions = LayoutOptions.Center
            };
            resubmit.Clicked += async (Object sender, EventArgs e) =>
            {
                var networkConnection = DependencyService.Get<INetworkConnection>();
                networkConnection.CheckNetworkConnection();
                string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
                if (networkStatus.Equals("Connected"))
                {
                    if (projectname.SelectedIndex == -1)
                    {
                        await DisplayAlert("Oh Snap!", "Please pick the project name", "ok");
                    }
                    else if (task.SelectedIndex == -1)
                    {
                        await DisplayAlert("Oh Snap!", "Please pick the Task", "Ok");

                    }
                    else if (hrs_picker.SelectedIndex == -1 && min_picker.SelectedIndex == -1)
                    {
                        await DisplayAlert("Oh Snap!", "Please enter the time", "ok");

                    }
                    else
                    {
                        var client = new HttpClient();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                        //  await DisplayAlert("Alert!", id_edit, "ok");
                        pick_project = projectname.Items[projectname.SelectedIndex];
                        pick_task = task.Items[task.SelectedIndex];
                        if (hrs_picker.SelectedIndex == -1)
                        {
                            hrs_value = "00";
                        }
                        else
                        {
                            hrs_value = hrs_picker.Items[hrs_picker.SelectedIndex];

                        }
                       
                        if (min_picker.SelectedIndex == -1)
                        {
                            min_value = "00";
                        }
                        else
                        {
                            min_value = min_picker.Items[min_picker.SelectedIndex];
                        }
                        time = hrs_value + ":" + min_value;
                        seconds_covert = TimeSpan.Parse(time).TotalSeconds.ToString();

                        desc = description.Text;
                        if (seconds_covert == "0")
                        {
                            await DisplayAlert("Invalid time!", "Time won't be 00:00.Atleast pick the minutes", "ok");
                        }
                        else
                        {
                            var postData = new List<KeyValuePair<string, string>>();

                            postData.Add(new KeyValuePair<string, string>(Constants.id, id_edit));
                            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                            postData.Add(new KeyValuePair<string, string>(Constants.hours, seconds_covert));
                            // postData.Add(new KeyValuePair<string, string>("date", date_edit));
                            postData.Add(new KeyValuePair<string, string>(Constants.task_description, desc));
                            postData.Add(new KeyValuePair<string, string>(Constants.project_name, pick_project));
                            postData.Add(new KeyValuePair<string, string>(Constants.task_name, pick_task));
                            postData.Add(new KeyValuePair<string, string>(Constants.approve_status, "Submitted"));
                            var content = new FormUrlEncodedContent(postData);
                            var response = await client.PostAsync(Constants.Edittimesheet_submit, content);
                            var JsonResult = response.Content.ReadAsStringAsync().Result;
                            if (JsonResult == "Timesheet updated successfully")
                            {


                                await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");
                             
                            }
                            MessagingCenter.Send(this, "MyItemsChanged");
                            Navigation.PopAsync();
                            // used for called module.
                            //Its useful for after calling Edit_Page module what could done can be define by this statement. 
                        }                                               //Through this statement can be dependends / connect with the other statement in their particular called module, due to that those module can be work
                    }
                }
                else
                {
                    await DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
                }
            };
            this.Content = new StackLayout
            {
                BackgroundColor = Color.FromHex("E0E0E0"),
                Children = {
                    new StackLayout
                    {
                        Padding=new Thickness(0,7,0,0),
                        Spacing=3,
                        Children=
                        {
                  new StackLayout
                        {
                      Padding=new Thickness(20,3,20,0),

                     Children=
                      {
                         new Frame
                         {

                             Content= new StackLayout
                             {
                                 Children=
                                 {
                                     p_lab,projectname,t_lab1,task
                                 }
                             }
                                 }

                      }
                        },new StackLayout {
                            Padding=new Thickness(20,5,20,0),
                            Children=
                            {
                                new Frame
                                {
                                    Content=new StackLayout
                                    {
                                        Children=
                                        {
                                            description
                                        }
                                    }
                                }
                            }

                        },new StackLayout
                        {
                            Padding=new Thickness(20,5,20,0),
                            Children=
                            {
                                new Frame
                                {
                                   Content=new StackLayout
                                   {
                                       Orientation=StackOrientation.Vertical,
                                       Children=
                                       {
                                           new StackLayout
                                           {
                                               Orientation=StackOrientation.Horizontal,
                                               HorizontalOptions=LayoutOptions.EndAndExpand,
                                               Children={
                                                     d_lab3,dateshow_lab,
                                               }

                                           },


                                                   dur_lab2,new StackLayout
                                                   {
                                                       Padding=new Thickness(150,0,0,0),
                                                       Orientation=StackOrientation.Horizontal,
                                                       Children=
                                                       {
                                                         hrs_picker,spli_time,min_picker
                                                       }
                                                   }
                                              
                                       

                                           //dur_lab2,time_dur
                                       }
                                   }
                                }
                            }


                        },new StackLayout
                        {
                            Padding =new Thickness(20,10,20,0),
                            Children=
                            {
                                resubmit
                            }
                        }
                   }

                    }



            }

            };


        }

        private async void LoadTask(string projectselected)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
          
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
            postData.Add(new KeyValuePair<string, string>(Constants.project_name, projectselected));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.task, content);
            //to take the value directly without the array 
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            var Items = JsonConvert.DeserializeObject<List<LoadSpinner>>(JsonResult);
            task.ItemsSource = Items;
            task.IsEnabled = true;
        }

        public async void LoadProject()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
          
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
            //postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            //postData.Add(new KeyValuePair<string, string>(Constants.approve_status, status_sel));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.project, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            string result = JsonResult.Trim(']').Trim('[');
            string[] project_split = result.Split(',');
            for (int i = 0; i < project_split.Length; i++)
            {
                string a = project_split[i];
                string b = a.Trim('"').Trim('"');
                projectname.Items.Add(b);
            }
            // var rootobject = JsonConvert.DeserializeObject<List<LoadSpinner>>(JsonResult.ToString());

            //picker.ItemsSource = rootobject;
            projectname.IsEnabled = true;
        }

        public async void LoadData()
        {


            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();

            postData.Add(new KeyValuePair<string, string>("id", ""));
            String hr = time_dur.Text;
            postData.Add(new KeyValuePair<string, string>("hours", hr));

            String pick_project = projectname.Items[projectname.SelectedIndex];
            String pick_task = task.Items[task.SelectedIndex];
            postData.Add(new KeyValuePair<string, string>("project_name", pick_project));
            postData.Add(new KeyValuePair<string, string>("task_name", pick_task));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.Edittimesheet_submit, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", "Thank you!" + JsonResult.ToString(), "ok");
        }
        IPlatformElementConfiguration<T, Frame> IElementConfiguration<Frame>.On<T>()
        {
            throw new NotImplementedException();
        }
        protected override bool OnBackButtonPressed()
        {

            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to submit the injury report before exit?", "Yes", "No");
                if (await result)
                {
                    Helpers.Settings.DisplayLoginstatus = true;
                   await Navigation.PushAsync(new Report_injury());
                }
                else
                {
                    Helpers.Settings.DisplayLoginstatus = true;
                    if (Device.OS == TargetPlatform.Android)

                        DependencyService.Get<IAndroidMethods>().CloseApp();

                    // base.OnBackButtonPressed();
                }

            });

            return true;




        }
        public void check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
            }
        }
    }
}