﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Before_previous_week : ContentPage
    {
        string emp_id, projectname, taskname, id, date, hours, status;
        string hrs_value, min_value, setcompid;

        private async void Submit_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;
            if (menuitem != null)
            {

                var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;

                if (visit != null)
                {
                    status = visit.approve_status.ToString();
                    if (status.Equals(Constants.approvestatus_submitted))
                    {
                        menuitem.IsDestructive = true;
                    }
                    if (status.Equals(Constants.approvestatus_reject) || status.Equals(Constants.approvestatus_saved))
                    {
                        var client = new HttpClient();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                        string id = visit.id.ToString();
                        var postData = new List<KeyValuePair<string, string>>();
                        postData.Add(new KeyValuePair<string, string>(Constants.id, id));
                        var content = new FormUrlEncodedContent(postData);
                        var response = await client.PostAsync(Constants.submit_saved_timesheet, content);
                        var JsonResult = response.Content.ReadAsStringAsync().Result;
                        await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");
                        filter_show1.IsVisible = false;
                        LoadData();
                    }
                }
            }
        }

        public Before_previous_week()
        {
            InitializeComponent();
            this.Title = "Two Weeks Back";
            check_connection();
            if (App.Current.Properties.ContainsKey("employeeid"))
                emp_id = (string)App.Current.Properties["employeeid"];
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
            if (string.IsNullOrEmpty(emp_id))
            {
                emp_id = Helpers.Settings.Displayemployeeid;
            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }

           // DisplayAlert("B Prevois week companyid", setcompid, "okay");
            MessagingCenter.Subscribe<Edit_Timesheet>(this, "MyItemsChanged", sender => {
                LoadData();
            });
            filter_show1.IsVisible = false;

            LoadData();

           
        }

        private void OnitemClicked_B(object sender, SelectedItemChangedEventArgs e)
        {
            var obj = (sender as ListView).SelectedItem as ItemClass;
            obj.itemcolor = Color.Blue;

        }

        //public void OnitemClicked_B(object sender, SelectedItemChangedEventArgs e)
        //{

        //    var obj = (sender as ListView).SelectedItem as ItemClass;
        //    obj.itemcolor = Color.Blue;
        //}
        private async void ImageClicked(object sender, EventArgs e)
        {
            //overlay.IsVisible = true;
            string statusselected;
            var action = await DisplayActionSheet("Filter the Timesheet?", "Cancel", null, "Submitted", "Saved", "Approved", "Rejected", "All");
            switch (action)
            {
                case "Submitted":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Saved":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Approved":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Rejected":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "All":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;

            }
        }

        private void into_clicked(object sender, EventArgs e)
        {
            overlay.IsVisible = false;

        }

        private async void filter_LoadData(string statusselected)
        {
            if (statusselected == "All")
            {

                filter_show1.IsVisible = true;
                filter_show1.Text = "All";
                LoadData();
            }
            else
            {

                filter_show1.IsVisible = true;
                filter_show1.Text = statusselected;
               
                string status_sel = statusselected;
                var client = new HttpClient();

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();
                postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                postData.Add(new KeyValuePair<string, string>(Constants.approve_status, status_sel));
                var content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.Filter_lastbeforeweek_status, content);
                var JsonResult = response.Content.ReadAsStringAsync().Result;
                if (string.IsNullOrEmpty(JsonResult))
                {
                    no_message.IsVisible = true;

                    no_message.Text = "No Timesheet is available";
                    no_message.TextColor = Color.Black;
                    listview123.IsVisible = false;
                }
                else
                {
                    string result = JsonResult.Trim(']').Trim('[');
                    var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());
                    listview123.IsVisible = true;
                   listview123.ItemsSource = rootobject;
                    overlay.IsVisible = false;
                    no_message.IsVisible = false;
                }
            }
        }
        public async void LoadData()
        {
            
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            var content = new FormUrlEncodedContent(postData);


            var response = await client.PostAsync(Constants.lastbeforeweek_timesheet, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;



            if (string.IsNullOrEmpty(JsonResult))
            {
                no_message.IsVisible = true;
                
                no_message.Text = "No Timesheet is available";
                no_message.TextColor = Color.Black;
                listview123.IsVisible = false;

            }
            else
            {


                var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());// Taken the List of array so only used <List<ItemClass>>
                listview123.IsVisible = true;
                no_message.IsVisible = false;
                listview123.ItemsSource = rootobject;
            }

        }
        private void Edit_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;
            if (menuitem != null)
            {

                var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;

                if (visit != null)
                {
                    projectname = visit.project_name.ToString();
                    taskname = visit.task_name.ToString();
                    id = visit.id.ToString();
                    date = visit.date.ToString();
                    hours = visit.Hourstring.ToString();
                    string[] hrs = hours.Split(':');

                    for (int i = 0; i < hrs.Length; i++)
                    {
                        hrs_value = hrs[0];
                        min_value = hrs[1];

                    }
                     status = visit.approve_status.ToString();
                    if (status.Equals(Constants.approvestatus_submitted))
                    {
                        menuitem.IsDestructive = true;
                    }
                    if (status.Equals(Constants.approvestatus_reject)||status.Equals(Constants.approvestatus_saved))
                    {
                        Navigation.PushAsync(new Edit_Timesheet(projectname, taskname, id, hrs_value, min_value, date));
                    }
                    if (status.Equals("Approved"))
                    {
                        return;
                    }
                }




            }


            filter_show1.IsVisible = false;
        }
        private async void Delete_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;
            if (menuitem != null)
            {

                var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;

                if (visit != null)
                {
                     status = visit.approve_status.ToString();
                    if (status.Equals(Constants.approvestatus_submitted))
                    {
                        menuitem.IsDestructive = true;
                    }
                    if (status.Equals(Constants.approvestatus_reject))
                    {
                        bool del = await DisplayAlert("Confirm?", "Would you like to delete the timesheet", "ok", "cancel");
                        if (del == true)
                        {
                            var client = new HttpClient();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                            id = visit.id.ToString();
                            var postData = new List<KeyValuePair<string, string>>();
                            postData.Add(new KeyValuePair<string, string>(Constants.id, id));
                            var content = new FormUrlEncodedContent(postData);
                            var response = await client.PostAsync(Constants.delete_timesheet, content);
                            var JsonResult = response.Content.ReadAsStringAsync().Result;
                            await DisplayAlert("alert!", JsonResult, "ok");
                            LoadData();
                        }
                        else
                        {
                            return;
                        }
                    }
                    if (status.Equals("Approved"))
                    {
                        return;
                    }
                    if (status.Equals(Constants.approvestatus_saved))
                    {
                        bool del = await DisplayAlert("Confirm?", "Would you like to delete the timesheet", "ok", "cancel");
                        if (del == true)
                        {
                            var client = new HttpClient();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                            id = visit.id.ToString();
                            var postData = new List<KeyValuePair<string, string>>();
                            postData.Add(new KeyValuePair<string, string>(Constants.id, id));
                            var content = new FormUrlEncodedContent(postData);
                            var response = await client.PostAsync(Constants.delete_timesheet, content);
                            var JsonResult = response.Content.ReadAsStringAsync().Result;
                            await DisplayAlert("Success!", "Thank you!! "+JsonResult, "ok");
                            LoadData();
                        }
                        else
                        {
                            return;
                        }
                    }


                }




            }



            filter_show1.IsVisible = false;
        }
        protected override bool OnBackButtonPressed()
        {

            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to submit the injury report before exit?", "Yes", "No");
                if (await result)
                {
                    await Navigation.PushAsync(new Report_injury());
                }
                else
                {
                    if (Device.OS == TargetPlatform.Android)

                        DependencyService.Get<IAndroidMethods>().CloseApp();

                    // base.OnBackButtonPressed();
                }

            });

            return true;




        }
        public void check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
            }
        }
        //private void isfiltering(object sender, EventArgs e)
        //{
        //    //filter1.IsVisible = false;

        //    status3.IsVisible = true;

        //}
    }
}



