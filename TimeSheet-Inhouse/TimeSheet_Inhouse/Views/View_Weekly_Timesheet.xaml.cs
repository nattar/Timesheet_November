﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class View_Weekly_Timesheet : TabbedPage
    {

        public View_Weekly_Timesheet()
        {
            InitializeComponent();
            this.Title = "View Weekly Timesheet";
            Children.Add(new Current_Week());
            Children.Add(new Previous_Week());
            Children.Add(new Before_previous_week());
        }
    }
}