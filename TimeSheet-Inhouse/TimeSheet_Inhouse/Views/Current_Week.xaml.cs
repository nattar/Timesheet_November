﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Current_Week : ContentPage
    {
        string emp_id, projectname, taskname, id, date, hours, status;
        private bool isRowEven;
        MenuItem menuitem;
        string hrs_value, min_value, dateformat, setcompid;
       
        int PreviousItemId = 0;
        string direction = "none";
       

        private async void Submit_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;
            if (menuitem != null)
            {

                var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;

                if (visit != null)
                {
                    status = visit.approve_status.ToString();
                    if (status.Equals(Constants.approvestatus_submitted))
                    {
                        menuitem.IsDestructive = true;
                    }
                    if (status.Equals(Constants.approvestatus_reject) || status.Equals(Constants.approvestatus_saved))
                    {
                        var client = new HttpClient();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                        string id = visit.id.ToString();
                        var postData = new List<KeyValuePair<string, string>>();
                        postData.Add(new KeyValuePair<string, string>(Constants.id, id));
                        var content = new FormUrlEncodedContent(postData);
                        var response = await client.PostAsync(Constants.submit_saved_timesheet, content);
                        var JsonResult = response.Content.ReadAsStringAsync().Result;
                        await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");
                        filter_show1.IsVisible = false;
                        // filter_show1.Text = "All";
                        listview1.IsVisible = false;
                        //listview1.ItemTapped += OnItemTapped;
                    
                        LoadData();
                    }
                }
            }
        }
        //private void OnItemTapped(object sender, ItemTappedEventArgs e)
        //{
        //    if (((ListView)sender).SelectedItem != null)
        //    {
        //        listview1.SelectedItem = null;
        //    }
        //}

        //private bool _isRefreshing = false;
        //public bool IsRefreshing
        //{
        //    get { return _isRefreshing; }
        //    set
        //    {
        //        _isRefreshing = value;
        //        OnPropertyChanged(nameof(IsRefreshing));
        //    }
        //}
        //public ICommand RefreshCommand
        //{
        //    get
        //    {
        //        return new Command(() =>
        //        {
        //            IsRefreshing = true;



        //            IsRefreshing = false;
        //        });
        //    }
        //}
        public Current_Week()
        {
            InitializeComponent();
            this.Title = "Current Week";
            check_connection();
            if (App.Current.Properties.ContainsKey("employeeid"))
                emp_id = (string)App.Current.Properties["employeeid"];
           
            if (App.Current.Properties.ContainsKey("Dateformat"))
                dateformat = (string)App.Current.Properties["Dateformat"];
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];


         
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }

            //DisplayAlert("Currentweek companyid", setcompid, "okay");
            MessagingCenter.Subscribe<Edit_Timesheet>(this, "MyItemsChanged", sender => {
                LoadData();
            });
            filter_show1.IsVisible = false;
            LoadData();
            
          

            listview1.ItemSelected += (object sender, SelectedItemChangedEventArgs e) =>
            {

               // ((AbsoluteLayout)e.SelectedItem).BackgroundColor = Color.AntiqueWhite;
               
            };
            

        }

        private void OnitemClicked_C(object sender, SelectedItemChangedEventArgs e)
        {
            var obj = (sender as ListView).SelectedItem as ItemClass;
            obj.itemcolor = Color.Blue;
        }

        private async void ImageClicked(object sender, EventArgs e)
        {
            //overlay.IsVisible = true;
            string statusselected;
            var action = await DisplayActionSheet("Filter the Timesheet?", "Cancel", null, "Submitted", "Saved", "Approved", "Rejected", "All");
            switch (action)
            {
                case "Submitted":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Saved":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Approved":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Rejected":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "All":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;

            }
        }

        private void into_clicked(object sender, EventArgs e)
        {
            overlay.IsVisible = false;
        }

        public async void LoadData()
        {

            //filter_show1.IsVisible = false;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            if (string.IsNullOrEmpty(emp_id))
            {
                emp_id = Helpers.Settings.Displayemployeeid;
            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }

            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            var content = new FormUrlEncodedContent(postData);            
            var response = await client.PostAsync(Constants.Current_week_timesheet, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            indicator.IsRunning = false;
            indicator.IsVisible = false;
            //   string result = JsonResult.Trim(']').Trim('[');
            if (string.IsNullOrEmpty(JsonResult))
            {
                no_message.IsVisible = true;

                no_message.Text = "No Timesheet is available";
                no_message.TextColor = Color.Black;
                listview1.IsVisible = false;
            }
            else
            {
                var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());
                listview1.IsVisible = true;
                no_message.IsVisible = false;
               
                listview1.ItemsSource = rootobject;
                listview1.ItemAppearing += (object sender, ItemVisibilityEventArgs e) =>
                  {


                  };
               
            }
          
           
        }



    
        public async void filter_LoadData(string sel_status)
        {
            if (sel_status == "All")
            {
                filter_show1.IsVisible = true;
                filter_show1.Text = "All";
                LoadData();

            }
            else
            {


                filter_show1.IsVisible = true;
                filter_show1.Text = sel_status;
                string status_sel = sel_status;
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();
                if (string.IsNullOrEmpty(emp_id))
                {
                    emp_id = Helpers.Settings.Displayemployeeid;
                }
                if (string.IsNullOrEmpty(setcompid))
                {
                    setcompid = Helpers.Settings.Displaycompanyid;
                }

                postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                postData.Add(new KeyValuePair<string, string>(Constants.approve_status, status_sel));
                var content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.Filter_thisweek_status, content);
                var JsonResult = response.Content.ReadAsStringAsync().Result;
                if (string.IsNullOrEmpty(JsonResult))
                {
                    no_message.IsVisible = true;

                    no_message.Text = "No Timesheet is available";
                    no_message.TextColor = Color.Black;
                    listview1.IsVisible = false;
                }
                else
                {
                    string result = JsonResult.Trim(']').Trim('[');
                    var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());
                    no_message.IsVisible = false;
                    listview1.IsVisible = true;
                   
                    listview1.ItemsSource = rootobject;
                    overlay.IsVisible = false;
                }
            }
        }

        private void Edit_Clicked(object sender, EventArgs e)
        {

            var menuitem = sender as MenuItem;

            if (menuitem != null)
            {


                var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;


                if (visit != null)
                {

                    projectname = visit.project_name.ToString();
                    taskname = visit.task_name.ToString();
                    id = visit.id.ToString();
                    //   date = visit.date.ToString("MM - dd - yyyy");
                    date = visit.date.ToString();
                    hours = visit.Hourstring.ToString();

                    status = visit.approve_status.ToString();
                    if (status.Equals(Constants.approvestatus_submitted))
                    {
                        menuitem.IsDestructive = true;
                    }
                    if (status.Equals(Constants.approvestatus_reject) || status.Equals(Constants.approvestatus_saved))
                    {
                        string[] hrs = hours.Split(':');
                        for (int i = 0; i < hrs.Length; i++)
                        {
                            hrs_value = hrs[0];
                            min_value = hrs[1];

                        }
                        Navigation.PushAsync(new Edit_Timesheet(projectname, taskname, id, hrs_value, min_value, date));
                    }
                    if (status.Equals("Approved"))
                    {
                        return;
                    }

                }




            }
            else
            {
                return;
            }



            filter_show1.IsVisible = false;
        }
        private async void Delete_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;

            if (menuitem != null)
            {

                var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;

                if (visit != null)
                {

                    status = visit.approve_status.ToString();
                    if (status.Equals(Constants.approvestatus_submitted))
                    {
                        menuitem.IsDestructive = true;
                    }
                    if (status.Equals(Constants.approvestatus_reject) || status.Equals(Constants.approvestatus_saved))
                    {
                        bool del = await DisplayAlert("Confirm?", "Would you like to delete the timesheet", "ok", "cancel");
                        if (del == true)
                        {
                            var client = new HttpClient();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                            id = visit.id.ToString();
                            var postData = new List<KeyValuePair<string, string>>();
                            postData.Add(new KeyValuePair<string, string>(Constants.id, id));
                            var content = new FormUrlEncodedContent(postData);
                            var response = await client.PostAsync(Constants.delete_timesheet, content);
                            var JsonResult = response.Content.ReadAsStringAsync().Result;
                            await DisplayAlert("alert!", JsonResult, "ok");
                            LoadData();
                        }
                        else
                        {
                            return;
                        }
                    }
                    if (status.Equals("Approved"))
                    {
                        return;
                    }
                }

            }
            filter_show1.IsVisible = false;
        }
        private void Cell_OnAppearing(object sender, EventArgs e)
        {
            if (this.isRowEven)
            {
                var viewCell = (ViewCell)sender;

                if (viewCell.View != null)
                {
                    viewCell.View.BackgroundColor = Color.Beige;
                }
            }

            this.isRowEven = !this.isRowEven;
        }

        protected override bool OnBackButtonPressed()
        {

            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to submit the injury report before exit?", "Yes", "No");
                if (await result)
                {
                    await Navigation.PushAsync(new Report_injury());
                }
                else
                {
                    if (Device.OS == TargetPlatform.Android)

                        DependencyService.Get<IAndroidMethods>().CloseApp();

                    // base.OnBackButtonPressed();
                }

            });

            return true;




        }
        public void check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
            }
        }
        //private void isfiltering(object sender, EventArgs e)
        //{
        //    //filter1.IsVisible = false;

        //    status1.IsVisible = true;

        //}
    }
}