﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailedviewChart : ContentPage
    {
        public int a, b, c, d;
        public DetailedviewChart()
        {
            InitializeComponent();
            this.Title = "Detailed Timesheet";


            if (App.Current.Properties.ContainsKey("save_t"))
                a = (Int32)App.Current.Properties["save_t"];
            if (App.Current.Properties.ContainsKey("sub_t"))
                b = (Int32)App.Current.Properties["sub_t"];
            if (App.Current.Properties.ContainsKey("appr_t"))
                c = (Int32)App.Current.Properties["appr_t"];
            if (App.Current.Properties.ContainsKey("rej_t"))
                d = (Int32)App.Current.Properties["rej_t"];
            check_connection();
            var sav = new Label
            {
                Text = "Total Saved Timesheet:",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };

            var sav_val = new Label
            {
                Text = a.ToString(),
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            var submit = new Label
            {
                Text = "Total Submitted Timesheet:",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            var sub_val = new Label
            {
                Text = b.ToString(),
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            var appr = new Label
            {
                Text = "Total Approved Timesheet:",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            var appr_val = new Label
            {
                Text = c.ToString(),
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            var reject = new Label
            {
                Text = "Total Rejected Timesheet:",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };

            var rej_val = new Label
            {
                Text = d.ToString(),
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            var sav_img = new Image { Source = "save.png" };
            var sub_imge = new Image { Source = "submit.png" };
            var appr_img = new Image { Source = "appr.png" };

            var rej_img = new Image { Source = "rejected.png" };



            this.Content = new StackLayout
            {
                BackgroundColor = Color.FromHex("E0E0E0"),
                Padding = new Thickness(20, 50, 20, 50),
                Children = {
                       new Frame
                        {
                                Content=new StackLayout
                                    {
                                    Orientation =StackOrientation.Vertical,

                                        Children=
                                        {        new StackLayout {
                            Orientation=StackOrientation.Horizontal,
                            Children=
                                 {
                                    sav_img,sav,sav_val
                                 }

                                        },
                                         new StackLayout {
                            Orientation=StackOrientation.Horizontal,
                            Children=
                                 {
                                    sub_imge,submit,sub_val
                                 }

                                        }, new StackLayout {
                            Orientation=StackOrientation.Horizontal,
                            Children=
                                 {
                                    appr_img,appr,appr_val
                                 }

                                        }, new StackLayout {
                            Orientation=StackOrientation.Horizontal,
                            Children=
                                 {
                                    rej_img,reject,rej_val
                                 }

                                        },
                                    }
                        } }
                }
            };
        }
        protected override bool OnBackButtonPressed()
        {

            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to submit the injury report before exit?", "Yes", "No");
                if (await result)
                {
                    await Navigation.PushAsync(new Report_injury());
                }
                else
                {
                    if (Device.OS == TargetPlatform.Android)

                        DependencyService.Get<IAndroidMethods>().CloseApp();

                    // base.OnBackButtonPressed();
                }

            });

            return true;




        }
        public void check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
            }
        }
    }
}