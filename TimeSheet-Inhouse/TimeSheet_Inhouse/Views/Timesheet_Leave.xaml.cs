﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;





namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Timesheet_Leave : ContentPage
    {
        string a, b, content, setempid, JsonResult, id_reject, reject_desc, setcompid;
        public Timesheet_Leave()
        {
            InitializeComponent();
            if (App.Current.Properties.ContainsKey("employeeid"))
                setempid = (string)App.Current.Properties["employeeid"];
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
            if (string.IsNullOrEmpty(setempid))
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
            //getdate();
            LoadData();
            listview_leave.ItemSelected += (object sender, SelectedItemChangedEventArgs e) =>
            {
                Itemleave item = (Itemleave)e.SelectedItem;
                string val = item.id;
            };

        }



        private void OnApprove(object sender, EventArgs e)
        {

            var item = (Xamarin.Forms.Image)sender;
            if (item != null)
            {
                var visit = (sender as Xamarin.Forms.Image).BindingContext as Itemleave;
                if (visit != null)
                {
                    string id = visit.id.ToString();
                    approve_leave(id);
                }
                else
                {
                    DisplayAlert("ok", "gfghhj", "ok");
                }
            }
            else
            {
                return;
            }


        }




        private void ok_clicked(object sender, EventArgs e)
        {
            string Rejected_id = id_reject;
            reject_desc = EnteredName.Text;

            overlay.IsVisible = false;
            Reject_leave(Rejected_id);
        }

        private void into_clicked(object sender, EventArgs e)
        {
            overlay.IsVisible = false;
        }

        private void OnReject(object sender, EventArgs e)
        {
            overlay.IsVisible = true;
            var item = (Xamarin.Forms.Image)sender;
            if (item != null)
            {
                var visit = (sender as Xamarin.Forms.Image).BindingContext as Itemleave;
                if (visit != null)
                {
                    id_reject = visit.id.ToString();
                    // DisplayAlert("ok", visit.Dateend.ToString(), "ok");

                }
                else
                {
                    DisplayAlert("ok", "gfghhj", "ok");
                }
            }
            else
            {
                return;
            }
        }

        public async void LoadData()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            //postData.Add(new KeyValuePair<string, string>(Constants.approve_status, status_sel));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_holiday, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            string result = JsonResult.Trim(']').Trim('[');
            //await DisplayAlert("okay ", result, "ok");
            var rootobject = JsonConvert.DeserializeObject<List<Itemleave>>(JsonResult.ToString());
            //await DisplayAlert("okay", rootobject.ToString(),"ok");
            listview_leave.ItemsSource = rootobject;
        }

        private async void approve_leave(string id)
        {

            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));


            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("id", id));


            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.approve_holiday, content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", JsonResult, "ok");
            LoadData();

        }
        private async void Reject_leave(string id)
        {

            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));


            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("id", id));


            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.Reject_holiday, content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", JsonResult, "ok");
            LoadData();

        }
        protected override bool OnBackButtonPressed()
        {

            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to submit the injury report before exit?", "Yes", "No");
                if (await result)
                {
                    await Navigation.PushAsync(new Report_injury());
                }
                else
                {
                    if (Device.OS == TargetPlatform.Android)

                        DependencyService.Get<IAndroidMethods>().CloseApp();

                    // base.OnBackButtonPressed();
                }

            });

            return true;




        }
        public void check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
            }
        }
    }
}