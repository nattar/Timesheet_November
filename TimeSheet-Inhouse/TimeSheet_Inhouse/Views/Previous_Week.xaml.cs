﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Previous_Week : ContentPage
    {

        string emp_id, projectname, taskname, id, date, hours, status;
        string get_status, hrs_value, min_value, dateformat, setcompid;

        private async void Submit_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;
            if (menuitem != null)
            {

                var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;

                if (visit != null)
                {
                    status = visit.approve_status.ToString();
                    if (status.Equals(Constants.approvestatus_submitted))
                    {
                        menuitem.IsDestructive = true;
                    }
                    if (status.Equals(Constants.approvestatus_reject) || status.Equals(Constants.approvestatus_saved))
                    {
                        var client = new HttpClient();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                        string id = visit.id.ToString();
                        var postData = new List<KeyValuePair<string, string>>();
                        postData.Add(new KeyValuePair<string, string>(Constants.id, id));
                        var content = new FormUrlEncodedContent(postData);
                        var response = await client.PostAsync(Constants.submit_saved_timesheet, content);
                        var JsonResult = response.Content.ReadAsStringAsync().Result;
                        await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");
                        filter_show1.IsVisible = false;
                        LoadData();
                    }
                }
            }
        }

        private void OnitemClicked_P(object sender, SelectedItemChangedEventArgs e)
        {

            var obj = (sender as ListView).SelectedItem as ItemClass;
            obj.itemcolor = Color.Blue;
        }

        private async void ImageClicked(object sender, EventArgs e)
        {
            //overlay.IsVisible = true;
            string statusselected;
            var action = await DisplayActionSheet("Filter the Timesheet?", "Cancel", null, "Submitted", "Saved", "Approved", "Rejected", "All");
            switch (action)
            {
                case "Submitted":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Saved":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Approved":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "Rejected":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;
                case "All":
                    statusselected = action;
                    filter_LoadData(statusselected);
                    break;

            }

        }

        //private void label_date21_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    var datelabel = sender as Label;

        //    if(datelabel!=null && (e.PropertyName == "Text"))
        //    {



        //    }
        //}

        private void into_clicked(object sender, EventArgs e)
        {
            overlay.IsVisible = false;
        }

        public Previous_Week()
        {
            InitializeComponent();
            this.Title = "Previous Week";
            check_connection();
            if (App.Current.Properties.ContainsKey("employeeid"))
                emp_id = (string)App.Current.Properties["employeeid"];
            if (App.Current.Properties.ContainsKey("Dateformat"))
                dateformat = (string)App.Current.Properties["Dateformat"];
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
            if (string.IsNullOrEmpty(emp_id))
            {
                emp_id = Helpers.Settings.Displayemployeeid;
            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }

          //  DisplayAlert("Prevois week companyid", setcompid, "okay");
            MessagingCenter.Subscribe<Edit_Timesheet>(this, "MyItemsChanged", sender => {
                LoadData();
            });
            filter_show1.IsVisible = false;
            LoadData();

            status2.IsVisible = false;
            status2.SelectedIndexChanged += (sender, args) =>
            {

                string statusselected = status2.Items[status2.SelectedIndex];


                filter_LoadData(statusselected);

            };
            listview12.ItemSelected += (object sender, SelectedItemChangedEventArgs e) =>
            {
                if (e.SelectedItem != null)
                {
                    var value = e.SelectedItem.ToString();

                }
            };
            {

            };


        }


      
        private async void filter_LoadData(string statusselected)
        {

           
            if (statusselected == "All")
            {
                filter_show1.IsVisible = true;
                filter_show1.Text = "All";
                LoadData();
            }
            else
            {
                filter_show1.IsVisible = true;
                filter_show1.Text = statusselected;
                string status_sel = statusselected;
                var client = new HttpClient();
              
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                var postData = new List<KeyValuePair<string, string>>();

                postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
                postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
                postData.Add(new KeyValuePair<string, string>(Constants.approve_status, status_sel));
                var content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(Constants.Filter_previousweek_status, content);
                var JsonResult = response.Content.ReadAsStringAsync().Result;
                if (string.IsNullOrEmpty(JsonResult))
                {
                    no_message.IsVisible = true;

                    no_message.Text = "No Timesheet is available";
                    no_message.TextColor = Color.Black;
                    listview12.IsVisible = false;
                }
                else
                {
                    string result = JsonResult.Trim(']').Trim('[');
                    var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());
                    listview12.IsVisible = true;
                   listview12.ItemsSource = rootobject;
                    overlay.IsVisible = false;
                    no_message.IsVisible = false;
                }
            }
        }
        public async void LoadData()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.previousweek_timesheet, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            //string result = JsonResult.Trim(']').Trim('[');
            if (string.IsNullOrEmpty(JsonResult))
            {
                no_message.IsVisible = true;

                no_message.Text = "No Timesheet is available";
                no_message.TextColor = Color.Black;
                listview12.IsVisible = false;
            }
            else
            {
                var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());
                listview12.ItemsSource = rootobject;
                no_message.IsVisible = false;
                listview12.IsVisible = true;
            }

        }

        private void Edit_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;
            if (menuitem != null)
            {

                var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;

                if (visit != null)
                {
                    projectname = visit.project_name.ToString();
                    taskname = visit.task_name.ToString();
                    id = visit.id.ToString();
                    date = visit.date.ToString();
                    hours = visit.Hourstring.ToString();
                    string[] hrs = hours.Split(':');

                    for (int i = 0; i < hrs.Length; i++)
                    {
                        hrs_value = hrs[0];
                        min_value = hrs[1];

                    }
                    status = visit.approve_status.ToString();

                    if (status.Equals("Submitted"))
                    {
                        menuitem.IsDestructive = true;
                    }
                    if (status.Equals("Rejected") || status.Equals("Saved"))
                    {
                        Navigation.PushAsync(new Edit_Timesheet(projectname, taskname, id, hrs_value, min_value, date));
                    }
                    if (status.Equals("Approved"))
                    {
                        return;
                    }

                }




            }

            filter_show1.IsVisible = false;
        }
        private async void Delete_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;
            if (menuitem != null)
            {
                //for taking the single label value 
                //  var visit1 = (sender as Xamarin.Forms.MenuItem).BindingContext as string;
                // DisplayAlert("More Context Action", visit1, "OK");
                //for taking the items from list
                var visit = (sender as Xamarin.Forms.MenuItem).BindingContext as ItemClass;
                //  var mi = ((MenuItem)sender);
                if (visit != null)
                {
                    status = visit.approve_status.ToString();
                    if (status.Equals("Submitted"))
                    {
                        menuitem.IsDestructive = true;
                    }
                    if (status.Equals("Rejected") || status.Equals("Saved"))
                    {
                        if (status.Equals("Rejected") || status.Equals("Saved"))
                        {
                            bool del = await DisplayAlert("Confirm?", "Would you like to delete the timesheet", "ok", "Cancel");
                            if (del == true)
                            {
                                var client = new HttpClient();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                                id = visit.id.ToString();
                                var postData = new List<KeyValuePair<string, string>>();
                                postData.Add(new KeyValuePair<string, string>(Constants.id, id));
                                var content = new FormUrlEncodedContent(postData);
                                var response = await client.PostAsync(Constants.delete_timesheet, content);
                                var JsonResult = response.Content.ReadAsStringAsync().Result;
                                await DisplayAlert("Success!", "Thank you!! "+JsonResult, "ok");
                                LoadData();
                            }
                            else
                            {
                                return;
                            }

                        }
                        if (status.Equals("Approved"))
                        {
                            return;
                        }


                    }

                }
            }
            filter_show1.IsVisible = false;

        }
        protected override bool OnBackButtonPressed()
        {

            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to submit the injury report before exit?", "Yes", "No");
                if (await result)
                {
                    await Navigation.PushAsync(new Report_injury());
                }
                else
                {
                    if (Device.OS == TargetPlatform.Android)

                        DependencyService.Get<IAndroidMethods>().CloseApp();

                    // base.OnBackButtonPressed();
                }

            });

            return true;




        }
        public void check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
            }
        }
        private void isfiltering(object sender, EventArgs e)
        {
            //filter2.IsVisible = false;

            status2.IsVisible = true;

        }
    }
}