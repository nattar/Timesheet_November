﻿using Android.Renderscripts;
using Java.Lang.Reflect;
using Java.Util.Logging;
using Newtonsoft.Json;
using Plugin.LocalNotifications.Abstractions;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.Helpers;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class DayTimesheet : ContentPage, IElementConfiguration<Frame>
    {
        Enter_timesheet ent = new Enter_timesheet();
        public Label header, colon, STimer, showdate, timer_label_creation, localpath_label, spli_time, curdatetxt, header3, header2;
        public Picker picker, task, hrs_picker, min_picker;
        string dateformat, dates, setempname, seconds_covert, setcompid, setempid, project_select, task_select, date, desc, min_value, hrs_value, time, JsonResult, concatminsec, date_store;
        Image timeimg, img_attach;
       

        int restrictCount = 2;
        string  value_check;//EMPLOYEE_ID,
        private const int BUTTON_BORDER_WIDTH = 1;
        Boolean dec_Mak = false;
      //  Boolean timer_check = false;// visiblityoftimer;

       // Boolean switchstatus;
        
  
        public Int32 BorderRadius { get; set; }

        //  string value;
        DateTime value;
        private Entry  Description, gettime, getmin, getsec;//stime,

        public DayTimesheet()
        {
            check_connection();
           
            //to set the Title in the navigationbar
            //App.IsLoggedIn = true;

            this.Title = "Day Timesheet";
            //Helpers.Settings.DisplayLoginstatus = true;
        
            //properties:
            //employeeid
            //companyid
            //Enter_timesheet ent_timesheet = new Enter_timesheet();

            if (App.Current.Properties.ContainsKey("employeeid"))
                setempid = (string)App.Current.Properties["employeeid"];
            if (string.IsNullOrEmpty(setempid))
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }


            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }

            if (App.Current.Properties.ContainsKey("Dateformat"))
                dateformat = (string)App.Current.Properties["Dateformat"];


            if (App.Current.Properties.ContainsKey("employeename"))
                setempname = (string)App.Current.Properties["employeename"];

              getdate();

            // setempid = arg;
            //    setempname = arg;
            //    setcompid = arg;



      

            MessagingCenter.Subscribe<Enter_timesheet, string>(this, "Dateformat", (sender, arg) =>
            {

                dateformat = arg;

            });
            MessagingCenter.Subscribe<Enter_timesheet, string>(this, "Currentdate", (sender, arg) =>
            {

                date= arg;
                App.Current.Properties["todaydate"] = date;

            });
           

           header = new Label
            {
                Text = "Project Name",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            header2 = new Label
            {
                Text = "Task Name",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            header3 = new Label
            {
                Text = "Duration",
                TextColor = Color.Black,
                // FontFamily = Device.OnPlatform(null, "Lobster-Regular.ttf#Lobster-Regular",  null),
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            Loaddetails();
            timeimg = new Image
            {
                Source = "timersmall.png",
                WidthRequest = 20,
                HeightRequest = 20

            };
            curdatetxt = new Label
            {
                Text = "Date",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };

            //gettime = new Entry
            //{
            //    TextColor = Color.Black,
            //    FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
            //    //IsVisible = false,
            //    Keyboard = Keyboard.Numeric

            //};
            colon = new Label
            {
                Text = ":",
                
            };
            getmin = new Entry
            {
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Entry)),
                HorizontalOptions = LayoutOptions.Fill,
                WidthRequest = 30,
                
                Keyboard = Keyboard.Numeric
                
            };


            img_attach = new Image
            {
                Source = "attach1.png",
                HeightRequest = 20,
                WidthRequest = 20
            };
            var tap_attach = new TapGestureRecognizer();
            //Binding events  by the tap_startImage_Tapped;
           
            //Associating tap events to the image buttons  
            img_attach.GestureRecognizers.Add(tap_attach);
            getsec = new Entry
            {
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Entry)),
                HorizontalOptions = LayoutOptions.Fill,
                WidthRequest = 30,
               
                Keyboard = Keyboard.Numeric
            };
            getmin.TextChanged += OnTextChanged;
            getsec.TextChanged += OnTextChangedsec;
            getmin.IsVisible = false;
            getsec.IsVisible = false;
            colon.IsVisible = false;
          


            Description = new Entry
            {
                Placeholder = "Task description"
            };

            //MessagingCenter.Subscribe<Enter_timesheet, string>(this, "Dateformat", (sender, arg) =>
            //{

            //    dateformat = arg;

            //});
            picker = new Picker
            {
                Title = "Choose project",

                VerticalOptions = LayoutOptions.CenterAndExpand,

            };
            picker.IsEnabled = false;
            picker.Items.Add("Choose project");
            LoadProject();
    
          

            task = new Picker
            {
               
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            task.IsEnabled = false;
            //picker.ItemDisplayBinding = new Binding(Constants.project_name);

            //To display the picker value from JSon
            //picker.ItemDisplayBinding = new Binding("project_name");
            picker.SelectedIndexChanged += (sender, args) => {
                string projectselected = picker.Items[picker.SelectedIndex];

                if(picker.SelectedIndex == 0)
                {
                   // DisplayAlert("Oh Snap!", "Please select the project", "ok");
                    task.IsEnabled = false;
                    task.Title = null;
                }
                else
                {
                    LoadTask(projectselected);
                   
                }
              

            };
            
           


            task.ItemDisplayBinding = new Binding(Constants.task_name);

            showdate = new Label
            {
                TextColor = Color.Black,
                //  Text = "Current Date:" + DateTime.Today.ToString("MM'-'dd'-'YYYY"),
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Center
            };
            //showdate.Text = "Current Date: " +date;
            //showdate.SetBinding(Label.TextProperty, "today");
            var summit = new Button
            {
                Text = "Submit",
                BackgroundColor = Color.FromHex("#0077b9"),
                TextColor = Color.White,

                BorderRadius = 20,
            };  
          
            hrs_picker = new Picker
            {
                Title = "00  ",
            };
            min_picker = new Picker
            {
                Title = "00  ",
                
            };
            min_picker.SelectedIndexChanged += (sender, args) => {
              // Title = "Min";
              
            };
          
            spli_time = new Label
            {
                Text = ":",
                VerticalOptions = LayoutOptions.Center
            };
            List<string> hrs = new List<string> { };

            for (int i = 0; i < 24; i++)
            {
                if (i < 10)
                {
                    hrs_picker.Items.Add("0" + i.ToString());
                }
                else
                {
                    hrs_picker.Items.Add(i.ToString());

                }


            }
            List<string> mins = new List<string> { };

            for (int i = 0; i < 60; i = i + 15)
            {
                if (i < 10)
                {
                    min_picker.Items.Add("0" + i.ToString());
                }
                else
                {
                    min_picker.Items.Add(i.ToString());

                }


            }
            var save = new Button
            {
                Text = "  Save  ",
                BackgroundColor = Color.FromHex("#0077b9"),
                TextColor = Color.White,
                BorderRadius = 20,

            };
            timer_label_creation = new Label
            {
                Text = "nothing",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            timer_label_creation.IsVisible = false;
            // timer label
            STimer = new Label
            {
                Text = "Start Timer",
                TextColor = Color.Black,
            };








            // Set the OnclickListener for Label
            var STimer_tap = new TapGestureRecognizer();
            STimer_tap.Tapped += (s, e) => {


                getmin.IsVisible = true;
                getsec.IsVisible = true;
                colon.IsVisible = true;
                getmin.IsEnabled = true;
                getsec.IsEnabled = true;

                getmin.Text = "00";
                getsec.Text = "00";
                hrs_picker.IsVisible = false;
                hrs_picker.IsEnabled = false;
                min_picker.IsVisible = false;
                min_picker.IsEnabled = false;
                spli_time.IsVisible = false;
                spli_time.IsEnabled = false;
               // timer_check = true;
                dec_Mak = true;

                Navigation.PushAsync(new Timer());
            };
            STimer.GestureRecognizers.Add(STimer_tap);

            // Set the UnderLine effects from Custom renderers
            STimer.Effects.Add(Effect.Resolve("MyCompany.FocusEffect"));

            // to get the value grom one page to another

            //MessagingCenter.Subscribe<HomePage, string>(this, "emplyeeid", (sender, arg) =>
            //{
            //    setempid = arg;
            //});


            //MessagingCenter.Subscribe<HomePage, string>(this, "emplyeename", (sender, arg) =>
            //{

            //    setempname = arg;

            //});

            //MessagingCenter.Subscribe<HomePage, string>(this, "companyid", (sender, arg) =>
            //{

            //    setcompid = arg;


            //});

           
            MessagingCenter.Subscribe<Timer, string>(this, "Hi", (sender, arg) =>
            {

               
                value_check = arg;
                string[] minsec = value_check.Split(':');
                //DisplayAlert("min", minsec[0].ToString(), "ok");
                //DisplayAlert("sec", minsec[1].ToString(), "ok");
              //  timer_check = true;
                getmin.Text = minsec[0] + " ";

                getsec.Text = minsec[1] + " ";

                 dec_Mak = method1();


             

                //gettime.IsVisible = true;
                //gettime.Text = value_check;
                //timer_label_creation.IsVisible = true;
                //timer_label_creation.Text = value_check;


            });

            //visiblityoftimer = Helpers.Settings.DisplayTimerstatus;

            //if (visiblityoftimer == true)
            //{
              
            //}
            //else
            //{
                // getmin.Text = "00";
                //  getsec.Text = "00";
               

            //}


            this.Content = new StackLayout
            {
                BackgroundColor = Color.FromHex("E0E0E0"),
                Children = {
                    new ScrollView() {
                          VerticalOptions = LayoutOptions.FillAndExpand,
                          Content=
               new StackLayout

               {

                Padding = new Thickness(0,7,0,0),
               Spacing=3,
                Children = {
                new StackLayout {
                     Padding=new Thickness(20,3,20,0),

                    Children= {
                //Setting the Frame
                new Frame {
                  BackgroundColor=Color.FromHex("FFFFFF"),
                    OutlineColor=Color.White,
                    HasShadow=true,

               Content= new StackLayout
                {
                         Padding = new Thickness(0,0,0,0),
                   Spacing=1,
                Children=
                    {
                         header,picker,header2,task
                    }
                }

                }
                    }
                },new StackLayout {
                    Padding=new Thickness(20,8,20,0),
                    Children=
                    {
                        new Frame
                        {
                                Content=new StackLayout
                                    {
                                        Children=
                                        {
                                           Description
                                        }
                                    }
                        }
                    }


                },
                       new StackLayout {
                    Padding=new Thickness(20,8,20,0),

                    Children= {
                   new Frame
                {
                 OutlineColor=Color.White,
                    HasShadow=true,
                  BackgroundColor=Color.FromHex("FFFFFF"),
                     Content= new StackLayout
                {
                         Padding = new Thickness(0,0,0,0),
                            Spacing=4,
                Children=
                    {
                             new StackLayout {
                                 Orientation=StackOrientation.Horizontal,

                                 Children= {
                            new StackLayout
                             {

                                HorizontalOptions=LayoutOptions.EndAndExpand,
                                 Children={
                                    showdate
                                 }
                                       }
                                 }
                             },
                             header3,new StackLayout {
                            Orientation=StackOrientation.Horizontal,
                            Children=
                                 {
                                     getmin,
                                       new StackLayout
                             {
                                Padding=new Thickness(0,10,0,0),
                               
                                 Children={
                                 colon
                                 }
                                       }, getsec,





                                 }
                             },
                             new StackLayout
                                           {
                                               Padding=new Thickness(150,0,0,0),
                                               Orientation=StackOrientation.Horizontal,
                                               Children=
                                               {
                                                   hrs_picker,spli_time,min_picker
                                               }
                                           }




                    }
                                  }
                }
                       }

                },
                new StackLayout {
                    Orientation=StackOrientation.Horizontal,
                    Padding=new Thickness(220,2,0,0),
                    Children= {
                       timeimg,

                        new StackLayout {
                            Padding=new Thickness(0,1,0,0),
                            Children=
                            {
                                STimer
                            }
                        }

                    }
                },
                // new StackLayout {
                //  Orientation=StackOrientation.Horizontal,
                //Padding=new Thickness(100,7,0,0),
                //  Children=
                //    {
                //       gallery_photo,camera_photo
                //    }
                //},
                new StackLayout {
                  Orientation=StackOrientation.Horizontal,
                Padding=new Thickness(100,7,0,0),
                  Children=
                    {
                       save,summit
                    }
                },

                   }
            }
                }
        }
            };


            //getmin.IsVisible = false;
            //colon.IsVisible = false;
            //getsec.IsVisible = false;




            save.Clicked += async (object sender, EventArgs e) =>
            {

                //if (picker.SelectedIndex == -1 && picker.SelectedIndex == 0)
                //{
                //    await DisplayAlert("Oh Snap!", "Please enter the project", "ok");
                //}
                if (picker.SelectedIndex == -1)
                {
                    await DisplayAlert("Oh Snap!", "Please enter the project", "ok");
                }
                else if (picker.SelectedIndex == 0)
                {
                    await DisplayAlert("Oh Snap!", "Please enter the project", "ok");
                }
                else
                {
                    if (task.SelectedIndex == -1)
                    {
                        await DisplayAlert("Oh Snap!", "Please enter the task", "ok");
                    }
                    //
                    else if ((hrs_picker.IsVisible==true)&&(min_picker.IsVisible==true))
                    {
                        if (hrs_picker.SelectedIndex == -1 && min_picker.SelectedIndex == -1)
                        {
                            await DisplayAlert("Oh Snap!", "Please pick the time", "ok");

                        }
                        else
                        {

                            await DisplayAlert("check", "picker", "ok");
                            if (hrs_picker.SelectedIndex == -1)
                            {
                                hrs_value = "00";
                            }
                            else
                            {
                                hrs_value = hrs_picker.Items[hrs_picker.SelectedIndex];
                            }
                            if (min_picker.SelectedIndex == -1)
                            {
                                min_value = "00";
                            }
                            else
                            {
                                min_value = min_picker.Items[min_picker.SelectedIndex];
                            }

                            time = hrs_value + ":" + min_value;
                            seconds_covert = TimeSpan.Parse(time).TotalSeconds.ToString();
                            desc = Description.Text;


                            try
                            {
                                if (seconds_covert == "0")
                                {
                                    await DisplayAlert("Invalid time!", "Time won't be 00:00.Atleast pick the minutes", "ok");
                                }
                                else
                                {
                                    go_Save_Submit("Saved", "entry", seconds_covert);
                                    ent.LoadTimesheet();
                                }
                            }
                            catch (Exception ex)
                            {

                            }



                        }
                    }
                    //
                    //-
                    else
                    {

                        //string getsec_val=
                        if (string.IsNullOrEmpty(getsec.Text) || string.IsNullOrEmpty(getmin.Text))
                        {
                            await DisplayAlert("Oh Snap!", "Please enter the value", "ok");
                        }
                      
                        else
                        {

                          //  await DisplayAlert("check", "Entry", "ok");
                            int min_data = Convert.ToInt32(getmin.Text);
                            int sec_data = Convert.ToInt32(getsec.Text);
                            if (min_data >= 24)
                            {
                                await DisplayAlert("Oh Snap!", "Please enter the value with in 23 hours", "ok");
                            }
                            else if (sec_data >= 60)
                            {
                                await DisplayAlert("Oh Snap!", "Please enter the value with in 60 minutes", "ok");
                            }

                            else
                            {


                               // await DisplayAlert("check_getmin", getmin.Text, "ok");
                               // await DisplayAlert("check_getsec", getsec.Text, "ok");
                                concatminsec = getmin.Text + ":" + getsec.Text;
                               // await DisplayAlert("check_concatmin", concatminsec, "ok");
                                seconds_covert = TimeSpan.Parse(concatminsec).TotalSeconds.ToString();
                               // await DisplayAlert("check_secon_convert", seconds_covert, "ok");
                                if (picker.SelectedIndex == -1)
                                {
                                    await DisplayAlert("Oh Snap!", "Please enter the project", "ok");
                                }
                                else if (task.SelectedIndex == -1)
                                {
                                    await DisplayAlert("Oh Snap!", "Please enter the task", "ok");
                                }
                                else if (dec_Mak == true)
                                {
                                    //  Save_data_using_timer("Saved",concatminsec);
                                    if (seconds_covert == "0")
                                    {
                                        await DisplayAlert("Invalid time!", "Time won't be 00:00.Atleast pick the minutes", "ok");
                                    }
                                    else
                                    {
                                        go_Save_Submit("Saved", "timer", seconds_covert);
                                        ent.LoadTimesheet();
                                    }
                                }
                                else
                                {

                                }
                            }
                        }
                    }
                    //-

                }



              


            };





            summit.Clicked += async (object sender, EventArgs e) =>
            {
                if (picker.SelectedIndex == -1)
                {
                    await DisplayAlert("Oh Snap!", "Please enter the project", "ok");
                }
                else if (picker.SelectedIndex == 0)
                {
                    await DisplayAlert("Oh Snap!", "Please enter the project", "ok");
                }

                else
                {
                    if (task.SelectedIndex == -1)
                    {
                        await DisplayAlert("Oh Snap!", "Please enter the task", "ok");
                    }

                    //-
                    else if ((hrs_picker.IsVisible == true) && (min_picker.IsVisible == true))


                    {

                      //  await DisplayAlert("check", "picker", "ok");
                        if (hrs_picker.SelectedIndex == -1 && min_picker.SelectedIndex == -1)
                        {
                            await DisplayAlert("Oh Snap!", "Please pick the time", "ok");

                        }
                        else
                        {


                            if (hrs_picker.SelectedIndex == -1)
                            {
                                hrs_value = "00";
                            }
                            else
                            {
                                hrs_value = hrs_picker.Items[hrs_picker.SelectedIndex];
                            }
                            // hrs_value = hrs_picker.Items[hrs_picker.SelectedIndex];
                            if (min_picker.SelectedIndex == -1)
                            {
                                min_value = "00";
                            }
                            else
                            {
                                min_value = min_picker.Items[min_picker.SelectedIndex];
                            }
                            time = hrs_value + ":" + min_value;
                            seconds_covert = TimeSpan.Parse(time).TotalSeconds.ToString();
                            desc = Description.Text;




                            try
                            {
                                if (seconds_covert == "0")
                                {
                                    DisplayAlert("Invalid time!", "Time won't be 00:00.Atleast pick the minutes", "ok");
                                }
                                else
                                {
                                    //SubmitData_using_Entry("Submitted",time);
                                    go_Save_Submit("Submitted", "entry", seconds_covert);
                                    ent.LoadTimesheet();
                                }
                            }
                            catch (Exception ex)
                            {

                            }



                        }
                    }
                    //-
                    //---


                    else
                    {
                        if (string.IsNullOrEmpty(getsec.Text) || string.IsNullOrEmpty(getmin.Text))
                        {
                            await DisplayAlert("Oh Snap!", "Please enter the value", "ok");
                        }
                        else
                        {

                            int min_data = Convert.ToInt32(getmin.Text);
                            int sec_data = Convert.ToInt32(getsec.Text);
                            if (min_data >= 24)
                            {
                                await DisplayAlert("Oh Snap!", "Please enter the value with in 23 hours", "ok");
                            }
                            else if (sec_data >= 60)
                            {
                                await DisplayAlert("Oh Snap!", "Please enter the value with in 60 minutes", "ok");
                            }
                            else
                            {



                             //   await DisplayAlert("check_getmin", getmin.Text, "ok");
                               // await DisplayAlert("check_getsec", getsec.Text, "ok");
                                concatminsec = getmin.Text + ":" + getsec.Text;
                              //  await DisplayAlert("check_concatmin", concatminsec, "ok");
                                seconds_covert = TimeSpan.Parse(concatminsec).TotalSeconds.ToString();
                                await DisplayAlert("check_secon_convert", seconds_covert, "ok");
                                //get_timer_value = timer_label_creation.Text;
                                if (picker.SelectedIndex == -1)
                                {
                                    await DisplayAlert("Oh Snap!", "Please enter the project", "ok");
                                }
                                else if (task.SelectedIndex == -1)
                                {
                                    await DisplayAlert("Oh Snap!", "Please enter the task", "ok");
                                }
                                else if (dec_Mak == true)
                                {
                                    if (seconds_covert == "0")
                                    {
                                        await DisplayAlert("Invalid time!", "Time won't be 00:00.Atleast pick the minutes", "ok");
                                    }
                                    else
                                    {
                                        //SubmitData_using_Timer("Submitted",concatminsec);
                                        go_Save_Submit("Submitted", "timer", seconds_covert);
                                        ent.LoadTimesheet();
                                    }

                                }
                                else
                                {

                                }
                            }
                        }
                    }
                    //---
                }

           


            };











        }






        private void OnTextChangedsec(object sender, TextChangedEventArgs e)
        {
            string val = getsec.Text;
            //String val = entry.Text; //Get Current Text

            if (val.Length > restrictCount)//If it is more than your character restriction
            {
                val = val.Remove(val.Length - 1);// Remove Last character 
                getsec.Text = val; //Set the Old value
            }
        }

        private void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            string val = getmin.Text;
            //String val = entry.Text; //Get Current Text

            if (val.Length > restrictCount )//If it is more than your character restriction
            {
                val = val.Remove(val.Length - 1);// Remove Last character 
                getmin.Text = val; //Set the Old value
            }
                       
        }
       
        private bool method1()
        {
            getmin.IsVisible = true;
            getsec.IsVisible = true;
            colon.IsVisible = true;
            hrs_picker.IsVisible = false;
            hrs_picker.IsEnabled = false;
            min_picker.IsVisible = false;
            min_picker.IsEnabled = false;
            spli_time.IsVisible = false;
            spli_time.IsEnabled = false;

            return true;
        }

        protected override bool OnBackButtonPressed()
        {

            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to submit the injury report before exit?", "Yes", "No");
                if (await result)
                {
                    Helpers.Settings.DisplayLoginstatus = true;
                    await Navigation.PushAsync(new Report_injury());
                }
                else
                {
                    string empname;
                    if (App.Current.Properties.ContainsKey("employeename"))
                    {
                       empname = (string)App.Current.Properties["employeename"];
                        Helpers.Settings.DisplayLoginstatus = true;
                        var a = Helpers.Settings.DisplayLoginstatus;
                     // await  DisplayAlert("Empname and status", empname+a, "ok");
                    }
                   
                 

                    if (Device.OS == TargetPlatform.Android)

                    DependencyService.Get<IAndroidMethods>().CloseApp();

                            // base.OnBackButtonPressed();
                }

            });

            return true;
            



        }
        
        IPlatformElementConfiguration<T, Frame> IElementConfiguration<Frame>.On<T>()
        {
            throw new NotImplementedException();
        }

        public async void LoadTask(string projectname)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
            postData.Add(new KeyValuePair<string, string>(Constants.project_name, projectname));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.task, content);
            //to take the value directly without the array 
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            var Items = JsonConvert.DeserializeObject<List<LoadSpinner>>(JsonResult);
            task.ItemsSource = Items;
            task.Title = "Choose task";
            task.IsEnabled = true;
        }
        public async void LoadProject()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
           
            
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.project, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            string result = JsonResult.Trim(']').Trim('[');
            string[] project_split = result.Split(',');
         for(int i=0;i< project_split.Length;i++)
            {
              string a= project_split[i];
                string b = a.Trim('"').Trim('"');
                picker.Items.Add(b);
            }
            // var rootobject = JsonConvert.DeserializeObject<List<LoadSpinner>>(JsonResult.ToString());

            //picker.ItemsSource = rootobject;
            picker.IsEnabled = true;
        }











        public async void Loaddetails()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            //  postData.Add(new KeyValuePair<string, string>("name", "bkimbmn"));
            postData.Add(new KeyValuePair<string, string>("company_id", setcompid));


            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_company_settings, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            //take the single element without the array can be used by this below line
            //  var rootobject = JsonConvert.DeserializeObject<Person>(JsonResult);
            //take  the single element with the array can be used by this below line
            // this is to take the values from the array it trim the array holder from the json result
            string result = JsonResult.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<Login_user_Details>(result.ToString());
            //to load the image from the database
           
            if (rootobject.date_formats == "dd-mm-yy")
            {
                dateformat = "dd-MM-yy";
            }
            else if (rootobject.date_formats == "dd/mm/yy")
            {
                dateformat = "dd/MM/yy";
            }
            else if (rootobject.date_formats == "mm-dd-yy")
            {
                dateformat = "MM-dd-yy";
            }
            else if (rootobject.date_formats == "mm/dd/yy")
            {
                dateformat = "MM/dd/yy";
            }
            else if (rootobject.date_formats == "yy-mm-dd")
            {
                dateformat = "yy-MM-dd";
            }
            else if (rootobject.date_formats == "yy/mm/dd")
            {
                dateformat = "yy/MM/dd";
            }
            else
            {

            }

        }

        public void method2()
        {
            picker.SelectedIndex = 0;

            Description.Text = null;
          //  getmin.IsEnabled = false;
            getmin.IsVisible = false;
          //  getsec.IsEnabled = false;
            getsec.IsVisible = false;
            colon.IsVisible = false;
           // colon.IsEnabled = false;           
            hrs_picker.IsVisible = true;
            hrs_picker.IsEnabled = true;
            min_picker.IsVisible = true;
            min_picker.IsEnabled = true;
            spli_time.IsVisible = true;
            spli_time.IsEnabled = true;
            hrs_picker.SelectedIndex = -1;
            min_picker.SelectedIndex = -1;

            STimer.IsVisible = true;
        }
        public async void getdate()
        {


            var content = "";
            HttpClient client = new HttpClient();
         //  var RestURL = Constants.Current_date;
         var RestURL = Constants.current_date;
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = response.Content.ReadAsStringAsync().Result;
            string result = content.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<Loadcurrentdate>(result.ToString());
            //var rootobject = JsonConvert.DeserializeObject<LoadDate>(result.ToString());
            // value = rootobject.date_str;
            // value = rootobject.Datestring;
            // string a  = rootobject.current.ToString(dateformat);
            //  value = rootobject.current;
            value = rootobject.Current;
            // var val1 = rootobject.Current.Date;
            //string a_date_value = val1.ToString(dateformat);
            DateTime dat = Convert.ToDateTime(value);

            // var curr_date = value.Date;
            // date_store = val1.ToString(Constants.dateconvert);
            //string a_date_value = val1.ToString(dateformat);
            //showdate.Text = "Current Date: " + a_date_value;
            //App.Current.Properties["todaydate"] = a_date_value;


            date_store = dat.ToString(Constants.dateconvert);
            string a_date_value = dat.ToString(dateformat);


            showdate.Text = "Current Date: " + a_date_value;
            App.Current.Properties["todaydate"] = a_date_value;
           





            //value = rootobject.Datestring;
            //showdate.Text = "Current Date: " + value.ToString(dateformat);
        }
        public void check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";

        }

        private async void go_Save_Submit(string a, string b, string get_timer_value)
        {

            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            project_select = picker.Items[picker.SelectedIndex];
            task_select = task.Items[task.SelectedIndex];
            //desc = Description.Text.ToString();
            //  await DisplayAlert("Date",a,value+b, "okay");

            //   string s = "\"";
            // value = s + value + s;
            // await DisplayAlert("Date", value, "okay");
            // DateTime oDate = Convert.ToDateTime(value);


            //  date = value.ToString(Constants.dateconvert);

            //    await DisplayAlert("Date", date, "okay");
          
            if (string.IsNullOrEmpty(setempid))
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;

            }


            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
            postData.Add(new KeyValuePair<string, string>(Constants.project_name, project_select));
            postData.Add(new KeyValuePair<string, string>(Constants.task_name, task_select));
            postData.Add(new KeyValuePair<string, string>(Constants.hours, get_timer_value));
            postData.Add(new KeyValuePair<string, string>(Constants.date,date_store));
            postData.Add(new KeyValuePair<string, string>(Constants.task_description, desc));
            string JsonResult = "Nothing";
          //  await DisplayAlert("Jsonresult", a, JsonResult + b, "okay");

            if (a == Constants.approvestatus_saved)
            {

                postData.Add(new KeyValuePair<string, string>(Constants.approve_status,Constants.approvestatus_saved));
                var content = new System.Net.Http.FormUrlEncodedContent(postData);
                var upload_url = Constants.save_timesheet;
                var response = await client.PostAsync(upload_url, content);
                JsonResult = response.Content.ReadAsStringAsync().Result;


            }
            if (a == Constants.approvestatus_submitted)
            {

                postData.Add(new KeyValuePair<string, string>(Constants.approve_status, Constants.approvestatus_submitted));
                var content = new System.Net.Http.FormUrlEncodedContent(postData);
                var upload_url = Constants.submit_timesheet;
                var response1 = await client.PostAsync(upload_url, content);
                JsonResult = response1.Content.ReadAsStringAsync().Result;


            }

            if (JsonResult == "Your Timesheet Saved Successfully")
            {


                await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");
                ent.LoadTimesheet();
            }
            else if (JsonResult == "Your Timesheet Submitted Successfully")
            {


                await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");
                ent.LoadTimesheet();
            }
            else
            {
                await DisplayAlert("Oh Snap!", "Sorry, " + JsonResult, "ok");
            }
            if (a == "Submitted" && b == "entry")



            {

                picker.SelectedIndex = 0;
                Description.Text = null;
                hrs_picker.SelectedIndex = -1;
                min_picker.SelectedIndex = -1;


                timeimg.IsVisible = true;
                STimer.IsVisible = true;
               

            }
            else if (a == "Submitted" && b == "timer")
            {
                method2();


                timeimg.IsVisible = true;
                STimer.IsVisible = true;

            }

            else if (a == "Saved" && b == "entry")
            {
                picker.SelectedIndex = 0;

                hrs_picker.SelectedIndex = -1;
                min_picker.SelectedIndex = -1;
                Description.Text = null;
            }
            
            else
            {
                method2();
            }


            dec_Mak = false;

        }

    }
}
