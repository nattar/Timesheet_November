﻿using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Report_injury : ContentPage
    {
        public Label header, header1, showdate, get_description, localpath_label, heading, attachment;
        public Picker picker;
        Image img_attach;
        string dateformat, setcompid, setempname, setempid;
        DatePicker date_picker;
        private Entry description;
        Button gallery_photo, camera_photo;
        private MediaFile _mediaFile;
        ActivityIndicator indicator;
        public Report_injury()
        {
            InitializeComponent();

            // alert_injury_report();

            // NavigationPage.SetHasNavigationBar(this, false);
            this.Title = "Injury Report";
            check_connection();



            if (App.Current.Properties.ContainsKey("employeeid"))
                setempid = (string)App.Current.Properties["employeeid"];
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
           if (App.Current.Properties.ContainsKey("Dateformat"))
               dateformat = (string)App.Current.Properties["Dateformat"];
            if (string.IsNullOrEmpty(setempid))
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
            //DisplayAlert("OK", EMPLOYEE_ID, "OK");
            // if (App.Current.Properties.ContainsKey("employeeid"))
            //  emp_id = (string)App.Current.Properties["employeeid"];
            //DisplayAlert("ok", setempid, "ok");
            getcurrentdate();
          //  getdate();

            gallery_photo = new Button()
            {
                Text = "Gallery",
                BackgroundColor = Color.Teal,
                TextColor = Color.White,

                BorderRadius = 20,
                HorizontalOptions = LayoutOptions.Center,
              //  HorizontalOptions = LayoutOptions.FillAndExpand,

            };
            camera_photo = new Button()
            {
                Text = "Camera",
                BackgroundColor = Color.Teal,
                TextColor = Color.White,

                BorderRadius = 20,
                 HorizontalOptions = LayoutOptions.Center,
               // HorizontalOptions = LayoutOptions.FillAndExpand,

            };
            gallery_photo.IsVisible = false;
            camera_photo.IsVisible = false;
            attachment = new Label
            {
                Text = "Attachment",
                TextColor = Color.Black,
            };
            attachment.Effects.Add(Effect.Resolve("MyCompany.FocusEffect"));
            localpath_label = new Label
            {
                Text = "",

                TextColor = Color.Black,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),

            };
            header = new Label
            {
                Text = "Project Name",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            var summit = new Button
            {
                Text = "Submit",
                BackgroundColor = Color.FromHex("#0077b9"),
                TextColor = Color.White,

                BorderRadius = 20,
                HorizontalOptions = LayoutOptions.Center

            };

            Label curdatetxt = new Label
            {
                Text = "Date",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                HorizontalOptions = LayoutOptions.StartAndExpand
            };

            indicator = new ActivityIndicator
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Color = Color.Black,
                IsVisible = false
            };


            img_attach = new Image
            {
                Source = "attach1.png",
                HeightRequest = 20,
                WidthRequest = 20
            };

            var tap_attach = new TapGestureRecognizer();
            //Binding events  by the tap_startImage_Tapped;
            tap_attach.Tapped += tap_attach_Tapped;
            //Associating tap events to the image buttons  
            attachment.GestureRecognizers.Add(tap_attach);

            date_picker = new DatePicker()
            {
                Format = dateformat,
                HorizontalOptions = LayoutOptions.End

            };
            header1 = new Label
            {
                Text = "Description",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HorizontalOptions = LayoutOptions.Start
            };
            description = new Entry
            {
                Placeholder = "Description"
            };



            LoadProject();


            picker = new Picker
            {
                Title = "Choose project",

                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            //To display the picker value from JSon
            picker.Items.Add("Choose project");



        
           
            gallery_photo.Clicked += (object sender, EventArgs e) =>
            {
                img_attach.IsVisible = true;
                attachment.IsVisible = true;
                gallery_photo.IsVisible = false;
                camera_photo.IsVisible = false;
                PickPhoto_Clicked();
                localpath_label.IsVisible = true;

            };
            camera_photo.Clicked += (object sender, EventArgs e) =>
            {
                img_attach.IsVisible = true;
                attachment.IsVisible = true;

                gallery_photo.IsVisible = false;
                camera_photo.IsVisible = false;
                TakePhoto_Clicked();
                localpath_label.IsVisible = true;
            };

            //MessagingCenter.Subscribe<HomePage, string>(this, "emplyeeid", (sender, arg) =>
            //{
            //    setempid = arg;

            //    // stime.Format = arg;
            //});

            //MessagingCenter.Subscribe<HomePage, string>(this, "emplyeename", (sender, arg) =>
            //{

            //    setempname = arg;

            //});

         
            summit.Clicked += async (object sender, EventArgs e) =>
            {


                if (picker.SelectedIndex == -1 || picker.SelectedIndex == 0)
                {
                    await DisplayAlert("Oh Snap!", "Please choose the project", "ok");
                }

                else
                {

                    try
                    {
                        //if (picker.SelectedIndex == 0)
                        //{
                        //    await DisplayAlert("Oh Snap!", "Please choose the project", "ok");
                        //}
                        //else {

                           
                        


                           // picker.IsEnabled = false;
                            img_attach.IsEnabled = false;
                            attachment.IsEnabled = false;
                            description.IsEnabled = false;
                          var client = new HttpClient();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                        string project_select = picker.Items[picker.SelectedIndex];
                            if (string.IsNullOrEmpty(setempid))
                            {
                                setempid = Helpers.Settings.Displayemployeeid;
                            }
                            if (string.IsNullOrEmpty(setcompid))
                            {
                                setcompid = Helpers.Settings.Displaycompanyid;
                            }
                            string date = date_picker.Date.ToString(Constants.dateconvert);

                        var desc = description.Text;
                        var content1 = new MultipartFormDataContent();
                        content1.Add(new StringContent(setcompid), Constants.company_id);
                        content1.Add(new StringContent(setempid), Constants.emp_id);
                        content1.Add(new StringContent(project_select), Constants.project_name);


                        if (!string.IsNullOrEmpty(desc))
                        {
                            content1.Add(new StringContent(desc), Constants.description);
                        }
                        content1.Add(new StringContent(date), Constants.date);

                        if (_mediaFile != null)
                        {
                                indicator.IsRunning = true;
                                indicator.IsVisible = true;
                                content1.Add(new StreamContent(_mediaFile.GetStream()), "\"injury_report\"", $"\"{_mediaFile.Path}\"");

                            var upload_url = Constants.injuryreport_submit;
                            var httpResponseMessage = await client.PostAsync(upload_url, content1);
                            var jsonvalue1 = await httpResponseMessage.Content.ReadAsStringAsync();
                            await DisplayAlert("Success", "" + jsonvalue1, "ok");

                            picker.SelectedIndex = 0;
                            _mediaFile = null;
                            getcurrentdate();
                            gallery_photo.IsVisible = false;
                            camera_photo.IsVisible = false;

                            localpath_label.IsVisible = false;
                            img_attach.IsVisible = true;
                            description.Text = null;
                                indicator.IsRunning = false;
                                indicator.IsVisible = false;
                            }
                        else
                        {
                            getcurrentdate();
                            await DisplayAlert("Oh Snap", "Please insert the attachment", "ok");
                                indicator.IsRunning = false;
                                indicator.IsVisible = false;
                            }


                          //  picker.IsEnabled =true;
                            img_attach.IsEnabled = true;
                            attachment.IsEnabled = true;
                            description.IsEnabled = true;
                           // summit.IsEnabled = false;
                            //   await   Navigation.PopToRootAsync();





                            //to take the value from the array it will be like the below lines
                            // string result = JsonResult.Trim(']').Trim('[');
                            //  var rootobject = JsonConvert.DeserializeObject<Person>(result.ToString());
                            //   var Status = rootobject.status;
                       // }
                    }
                    catch (Exception ex) { }



                }
            };
            this.Content = new StackLayout
            {
                BackgroundColor = Color.FromHex("E0E0E0"),
                Children = {
               new StackLayout
            {

                Padding = new Thickness(0,7,0,0),
              Spacing=3,
                Children = {
                new StackLayout {
                     Padding=new Thickness(20,3,20,0),

                    Children= {
                        //Setting the Frame
                new Frame {
                  BackgroundColor=Color.FromHex("FFFFFF"),
                    OutlineColor=Color.White,
                    HasShadow=true,

               Content= new StackLayout
                {
                         Padding = new Thickness(0,0,0,0),
                   Spacing=1,
                Children=
                    {
                         header,picker,
                    }
                }

                }
                    }
                },new StackLayout {
                    Padding=new Thickness(20,8,20,0),
                    Children=
                    {
                        new Frame
                        {
                OutlineColor=Color.White,
                    HasShadow=true,
                  BackgroundColor=Color.FromHex("FFFFFF"),
                  Content=new StackLayout
                  {
                         Padding = new Thickness(0,0,0,0),
                            Spacing=4,
                            Children=
                      {
                            description
                      }
                  }
                        }
                    }

                },new StackLayout {
                    Padding=new Thickness(20,8,20,0),

                    Children= {
                   new Frame
                {
                 OutlineColor=Color.White,
                    HasShadow=true,

                  BackgroundColor=Color.FromHex("FFFFFF"),
                     Content= new StackLayout
                {
                         Padding = new Thickness(0,0,0,0),
                            Spacing=4,
                Children=
                    {
                             new StackLayout {
                                 Orientation=StackOrientation.Horizontal,

                                 Children= {

                              curdatetxt,date_picker


                                 }
                             },






                    }
                                  }
                }
                       }

                },


                 new StackLayout {
                    Orientation=StackOrientation.Horizontal,
                    Padding=new Thickness(200,2,0,0),
                    Children= {
                     img_attach,
                        new StackLayout {
                            Padding=new Thickness(0,1,0,0),
                            Children=
                            {
                             attachment
                            }
                        }

                    }
                },

                 new  StackLayout {

                    Orientation=StackOrientation.Horizontal,
                   // Padding=new Thickness(35,10,0,0),
                   Padding=new Thickness(35,10,0,0),
                    Children= {
                      localpath_label,
                    }
                },
                new StackLayout {
                    
                    Orientation=StackOrientation.Horizontal,
                    Padding=new Thickness(100,10,0,0),
                    Children= {
                       gallery_photo,camera_photo,
                    }
                },

                       new StackLayout {
                  Orientation=StackOrientation.Horizontal,

               Padding=new Thickness(130,10,0,0),
                  Children=
                    {
                     summit
                    }
                },
                         new StackLayout {
                  Orientation=StackOrientation.Horizontal,
                Padding=new Thickness(0,10,0,0),
                  Children=
                    {
                  indicator
                    }
                },
                   }
            }
        }
            };
        }

        private void tap_attach_Tapped(object sender, EventArgs e)
        {
            img_attach.IsVisible = false;
            attachment.IsVisible = false;
            localpath_label.IsVisible = false;
            gallery_photo.IsVisible = true;
            camera_photo.IsVisible = true;
            
        }
        private async void PickPhoto_Clicked()
        {
            await CrossMedia.Current.Initialize();
          

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await DisplayAlert("No PickPhoto", ":( No PickPhoto available.", "OK");
                return;
            }

            _mediaFile = await CrossMedia.Current.PickPhotoAsync();

            if (_mediaFile == null)
                return;
            string img_name = Path.GetFileName(_mediaFile.Path);


            localpath_label.Text = img_name;
            // await DisplayAlert("File Location12324343", _mediaFile.Path, "OK");

            //localpath_label.Text = _mediaFile.Path;
            ImageSource.FromStream(() =>
            {
                return _mediaFile.GetStream();
            });
        }
        async void TakePhoto_Clicked()
        {
            
            string set_image_name, im, con_img;
            set_image_name = setempid;
            im = "_injuryreport.jpg";
            con_img = setempid + im;
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;

            }

            _mediaFile = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                //SaveToAlbum = true,// default camera role
                Directory = "Sample",

                Name = con_img,
            });
            if (_mediaFile == null)
                return;
            string img_name = Path.GetFileName(_mediaFile.Path);


            localpath_label.Text = img_name;
            //await DisplayAlert("File Location", _mediaFile.Path, "OK");
            //localpath_label.Text = _mediaFile.Path;
            ImageSource.FromStream(() => _mediaFile.GetStream());
            // fileimage.Source = ImageSource.FromStream(() => _mediaFile.GetStream());
        }


        public async void LoadProject()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            //postData.Add(new KeyValuePair<string, string>(Constants.emp_id, emp_id));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            //postData.Add(new KeyValuePair<string, string>(Constants.approve_status, status_sel));
            //postData.Add(new KeyValuePair<string, string>(Constants.approve_status, status_sel));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.project, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            string result = JsonResult.Trim(']').Trim('[');
            string[] project_split = result.Split(',');
            for (int i = 0; i < project_split.Length; i++)
            {
                string a = project_split[i];
                string b = a.Trim('"').Trim('"');
                picker.Items.Add(b);
            }
        }
        public async void getdate()
        {
            var content = "";
            HttpClient client = new HttpClient();

            var RestURL = Constants.getting_Week_dates;
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = response.Content.ReadAsStringAsync().Result;
            string result = content.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<LoadDate>(result.ToString());
            date_picker.MaximumDate = rootobject.Next;
            date_picker.MinimumDate = rootobject.Previous;
            // date_picker.Date = rootobject.current;
        }




        public async void getcurrentdate()
        {
            var content = "";
            HttpClient client = new HttpClient();

            var RestURL = Constants.current_date;
            client.BaseAddress = new Uri(RestURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            HttpResponseMessage response = await client.GetAsync(RestURL);
            content = response.Content.ReadAsStringAsync().Result;
            string result = content.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<Loadcurrentdate>(result.ToString());

            date_picker.Date = rootobject.Current;
            // date_picker.Date = rootobject.current;
        }
        //protected override bool OnBackButtonPressed()
        //{
        //    return true;
        //}
        public void check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
            }
        }



        protected override bool OnBackButtonPressed()
        {
            //MessagingCenter.Subscribe<Logedout, string>(this, "loggedout", (sender, arg) =>
            //{


            //    OnBackButtonPressed();

            //});

            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to exit the application?", "ok", "cancel");
                if (await result)
                {
                    if (Device.OS == TargetPlatform.Android)

                        DependencyService.Get<IAndroidMethods>().CloseApp();
                }
                else
                {
                    return;
                }
            });
            //base.OnBackButtonPressed();
            return true;
        }
















    }
}