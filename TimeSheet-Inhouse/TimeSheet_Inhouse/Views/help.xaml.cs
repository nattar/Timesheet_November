﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TimeSheet_Inhouse.Helpers;
namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class help : ContentPage

    {
        ViewModel.ListContentViewModel listcontentVM;
        public help()
        {
            InitializeComponent();
            this.Title = "Help";

            //Settings.isLoggedIn = true;
            listcontentVM = new ViewModel.ListContentViewModel();

            listforlistcontent.ItemsSource = listcontentVM.listcontent1;
            listforlistcontent.ItemTapped += OnItemTapped;

        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (((ListView)sender).SelectedItem != null)
            {
                listforlistcontent.SelectedItem = null;
            }
        }
        protected override bool OnBackButtonPressed()
        {

            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to submit the injury report before exit?", "Yes", "No");
                if (await result)
                {
                    await Navigation.PushAsync(new Report_injury());
                }
                else
                {
                    if (Device.OS == TargetPlatform.Android)

                        DependencyService.Get<IAndroidMethods>().CloseApp();

                    // base.OnBackButtonPressed();
                }

            });

            return true;




        }

    }
}
