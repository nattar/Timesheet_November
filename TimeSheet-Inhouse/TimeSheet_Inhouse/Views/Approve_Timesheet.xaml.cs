﻿using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Approve_Timesheet : ContentPage
    {
      
      
      
       
        string setempid,user_id, JsonResult, id_reject,reject_desc, setcompid;
        public Approve_Timesheet()
        {
            InitializeComponent();
           // Title = "Timesheet";
            
            if (App.Current.Properties.ContainsKey("employeeid"))
                setempid = (string)App.Current.Properties["employeeid"];
            if (App.Current.Properties.ContainsKey("companyid"))
                setcompid = (string)App.Current.Properties["companyid"];
            if (string.IsNullOrEmpty(setempid))
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }

            check_connection();
            Loademployee();
    
            employee.SelectedIndexChanged += (sender, args) =>
            {

                string employeeselected = employee.Items[employee.SelectedIndex];
                string[] id = employeeselected.Split('-');
                foreach (string getid in id)
                {
                    user_id = id[1];
                   
                }
                LoadTimesheet(user_id);
            };


        }

        public async void Loademployee()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));

            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_employee, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            string result = JsonResult.Trim(']').Trim('[');
      
            string[] tokens = result.Split(',');

            for (int i = 0; i < tokens.Length; i++)
            {
              string   a = tokens[i];
               string b = a.Trim('"').Trim('"');
                employee.Items.Add(b);
            }
        }


       
       

       

        public async void LoadTimesheet(string userid)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, userid));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_pending_timesheet, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            //   string result = JsonResult.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<List<ItemClass>>(JsonResult.ToString());
            listview12.ItemsSource = rootobject;
        }

        private void Approved(object sender, EventArgs e)
        {
            var item = (Xamarin.Forms.Image)sender;
            if (item != null)
            {
                var visit = (sender as Xamarin.Forms.Image).BindingContext as ItemClass;
                if (visit != null)
                {
                    string id = visit.id.ToString();
                    approve_timesheet(id);
                }
                else
                {
                    DisplayAlert("OhSnap!", "No value", "ok");
                }
            }
            else
            {
                return;
            }
        }

        private void into_clicked(object sender, EventArgs e)
        {
            overlay.IsVisible = false;
        }

        private void Rejected(object sender, EventArgs e)
        {
          
            overlay.IsVisible = true;
            var item = (Xamarin.Forms.Image)sender;
            if (item != null)
            {
                var visit = (sender as Xamarin.Forms.Image).BindingContext as ItemClass;
                if (visit != null)
                {
                    
                     id_reject = visit.id.ToString();
                   
                    //Reject_Timesheet(id);
                }
                else
                {
                    DisplayAlert("ok", "gfghhj", "ok");
                }
            }
            else
            {
                return;
            }
        }

       

        private void ok_clicked(object sender, EventArgs e)
        {
            string Rejected_id = id_reject;
            reject_desc = EnteredName.Text;
           
            overlay.IsVisible = false;
            Reject_Timesheet(Rejected_id);
        }

        //private void OnCancelButtonClicked(object sender, EventArgs e)
        //{
        //    overlay.IsVisible = false;
        //}

        //private void OnOKButtonClicked(object sender, EventArgs e)
        //{
        //    overlay.IsVisible = false;
        //    DisplayAlert("Result", "okay", "OK");
        //}

        private async void approve_timesheet(string id)
        {

            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));


            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("id", id));
            postData.Add(new KeyValuePair<string, string>("approved_by", setempid));

            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.Approve_timesheet, content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", JsonResult, "ok");
            LoadTimesheet(user_id);


            Enter_timesheet ent = new Enter_timesheet();
            ent.LoadTimesheet();

        }
        private async void Reject_Timesheet(string id)
        {
           
            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));


            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("id", id));
            postData.Add(new KeyValuePair<string, string>("approved_by", setempid));
            postData.Add(new KeyValuePair<string, string>("rejected_desc", reject_desc));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.Reject_timesheet, content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", JsonResult, "ok");
            LoadTimesheet(user_id);

        }
        protected override bool OnBackButtonPressed()
        {

            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to submit the injury report before exit?", "Yes", "No");
                if (await result)
                {
                    await Navigation.PushAsync(new Report_injury());
                }
                else
                {
                    if (Device.OS == TargetPlatform.Android)

                        DependencyService.Get<IAndroidMethods>().CloseApp();

                    // base.OnBackButtonPressed();
                }

            });

            return true;




        }
        public void check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
            }
        }

    }
}