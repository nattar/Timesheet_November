﻿using Android.Locations;
using Newtonsoft.Json;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TimeSheet_Inhouse.Helpers;
using TimeSheet_Inhouse.MenuItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class checkinxaml : ContentPage
    {
        string setempid, empname, setcompid,get_id, dateformat, Addressselected, add_get, eventid, JsonResult, result, latitude, longitude,  getnotes, eve_id, a1, b1;
        string[] tokens;
        public int n = 20;
        // Normal button height
        private const int BUTTON_HEIGHT = 66;
        private const int BUTTON_HEIGHT_WP = 144;
        private const int BUTTON_HALF_HEIGHT = 44;
        private const int BUTTON_HALF_HEIGHT_WP = 72;
        private const int BUTTON_WIDTH = 120;
        private const int BUTTON_WIDTH_WP = 144;
        private const int BUTTON_BORDER_WIDTH = 50;
   
        bool btn_status;
        Boolean getval;
        ContentView contentview;
        
        public Int32 BorderRadius { get; set; }

        public checkinxaml()
        {
            InitializeComponent();
            Title = "Checkin/out";
            if (App.Current.Properties.ContainsKey("employeeid"))
            {
                setempid = (string)App.Current.Properties["employeeid"];

            }
               
            else
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }
            if (App.Current.Properties.ContainsKey("companyid"))
            {
                setcompid = (string)App.Current.Properties["companyid"];
            }
            
            else
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
            if (App.Current.Properties.ContainsKey("Dateformat"))
            {
                dateformat = (string)App.Current.Properties["Dateformat"];
            }
                
            else
            {
                DisplayAlert("dateformat", "error", "okay");
            }
            if (string.IsNullOrEmpty(setempid))
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }
            else
            {

            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }else
            {
                //DisplayAlert(" setcompid", "error", "okay");
            }
            check_connection();

            //time-> date here, date->time
            time.Text = (string)App.Current.Properties["todaydate"];
            getlatlong();
            // time.Text = DateTime.Now.ToString(dateformat);

            date.Text = DateTime.Now.ToString("HH:mm tt");
            //btn_check_out.IsVisible = false;
           getval = Helpers.Settings.Displaybuttonstatus;
            //DisplayAlert("ok", getval.ToString(), "ok");
           
            if (getval == true)
            {
                btn_check.Text = "Check-out";
               
            }
            else
            {

            }
            if(getval==false)
            {
           
            btn_check.Text = "Check-in";
            
            }
            else
            {

            }
          
            checklocconnect();
            Address.Items.Add("Choose Address");
          

            var Add_tap = new TapGestureRecognizer();
            Add_tap.Tapped += (s, e) => {

                Add.IsVisible = true ;
                
                overlay_checkin.IsVisible = true;
                title.Text = "  Add a Address";
                EnteredName.Placeholder = "Add a Address";

                btn_addedit.Text = "Add";
                btn_addedit.IsEnabled = false;
                if (EnteredName != null)
                {
                    btn_addedit.IsEnabled = true;
                }
                else
                {
                    btn_addedit.IsEnabled = false;
                }

               
            };
            Address.SelectedIndexChanged += (sender, args) =>
            {
                Delete.IsVisible = true;
                Delete.IsEnabled = true;
                slash.IsVisible = true;
                slash.IsEnabled = true;
                Edit.IsVisible = true;
                Edit.IsEnabled = true;

                if (Address.SelectedIndex == 0)
                {
                    //    DisplayAlert("OhSnap!", "Please select your given address", "okay");
                    //    Loadaddress();
                }
                else
                {
                    Addressselected = Address.Items[Address.SelectedIndex];

                    if (Addressselected == "Choose Address")
                    {

                        DisplayAlert("OhSnap!", "Please select your given address", "okay");
                        Loadaddress();
                    }

                    else if (Addressselected == add_get)
                    {
                        Addressselected = Addressselected;
                    }


                    else
                    {
                        if (tokens.Length == 0)
                        {
                            return;
                        }
                        else
                        {
                            for (int i = 0; i < tokens.Length; i++)
                            {
                                string a = tokens[i];
                                string b = a.Trim('"').Trim('"');
                                string[] id = b.Split('-');
                                string Add_id = id[0];
                                string Add_name = id[1];
                                if (Add_name == Addressselected)
                                {
                                    get_id = id[0];
                                }
                            }
                        }
                    }
                    if (Addressselected == add_get)
                    {
                        Delete.IsVisible = false;
                        slash.IsVisible = false;
                        Edit.IsVisible = false;
                    }
                    else
                    {

                    }


                }

            };
            Add.GestureRecognizers.Add(Add_tap);
            Add.Effects.Add(Effect.Resolve("MyCompany.FocusEffect"));

           
            var Delete_tap = new TapGestureRecognizer();
            Delete_tap.Tapped += async (s, e) =>
            {


                if (Address.SelectedIndex == 0)
                {
                   // await DisplayAlert("Oh Snap!", "Please add the address", "ok");
                }
                else
                {


                    bool ans = await DisplayAlert("Confirm?", "Would you like to delete the "+ Addressselected , "Ok", "Cancel");
                    if (ans == true)
                    {
                        var client = new HttpClient();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
                        var postData = new List<KeyValuePair<string, string>>();
                        postData.Add(new KeyValuePair<string, string>(Constants.id, get_id));
                        var content = new System.Net.Http.FormUrlEncodedContent(postData);
                        var response = await client.PostAsync(Constants.Delete_address, content);
                        //to take the value direstly without the array 
                        JsonResult = response.Content.ReadAsStringAsync().Result;
                  
                        Address.SelectedIndex = 0;
                        Loadaddress();
                        await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");

                    }
                    else
                    {
                        return;
                    }
                    Loadaddress();
                }              
               
            };      
           Delete.GestureRecognizers.Add(Delete_tap);
            Delete.Effects.Add(Effect.Resolve("MyCompany.FocusEffect"));
            var Edit_tap = new TapGestureRecognizer();
            Edit_tap.Tapped += (s, e) => {

                if (Address.SelectedIndex == 0)
                {
                  //  DisplayAlert("Oh Snap!", "Please select your given address", "ok");
                }
                else
                {
                    overlay_checkin.IsVisible = true;
                    EnteredName.Text = Addressselected;
                    title.Text = "  Edit a Address";
                    EnteredName.Placeholder = "Edit a Address";
                    btn_addedit.Text = "Edit";
                }
             
            };    
            
                    
        
            Loadaddress();
            Edit.GestureRecognizers.Add(Edit_tap);
            Edit.Effects.Add(Effect.Resolve("MyCompany.FocusEffect"));
            btn_check.BorderRadius = 20;
           Java.Lang.Thread.Sleep(3000);
            checklocconnect();
        }       
        public void checklocconnect()
        {
            var locator = CrossGeolocator.Current;
            if (!locator.IsGeolocationEnabled)
            {

                ////TODO: TEST CODE
                #region LOCATION PART

                ////TODO: FAKE:
                MessagingCenter.Send(new Message(true, n), string.Empty);
                //return;

                #endregion
                btn_check.IsEnabled = false;


            }
            else
            {

            }
            if (locator.IsGeolocationEnabled)
            {
                Java.Lang.Thread.Sleep(1000);             
                if (getval == true)
                {
                    btn_check.Text = "Check-out";
                }
                else
                {

                }
                if (getval == false)
                {

                    btn_check.Text = "Check-in";

                }
                else
                {

                }
            }
            else
            {

            }
        }
        private void btn_check_send(object sender, EventArgs e)
        {
            if (Address.SelectedIndex == -1)
            {
                //DisplayAlert("OhSnap!", "Address field is missing", "ok");
            }
            else
            {
                Addressselected = Address.Items[Address.SelectedIndex];
                //getaddress = Address.Text;
                string btn_value = btn_check.Text.ToString();
                if (btn_value.Equals("Check-in"))
                {
                    checkin(latitude, longitude);
                }
                else if (btn_value.Equals("Check-out"))
                {
                    geteventid();
                }
            }           
        }
        private async void checkin(string lat, string longitude)
        {
            // getaddress = Address.Text;
            // getnotes = Notes.Text.ToString();
            if (Addressselected.Equals("Choose Address"))
            {
                await DisplayAlert("OhSnap!", "Please select valid address", "ok");
            }
            else
            {

            
            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();

                if (string.IsNullOrEmpty(setempid))
                {
                    setempid = Helpers.Settings.Displayemployeeid;
                }
                if (string.IsNullOrEmpty(setcompid))
                {
                    setcompid = Helpers.Settings.Displaycompanyid;
                }
           // await DisplayAlert("check in  companyid", setcompid, "okay");
            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            postData.Add(new KeyValuePair<string, string>(Constants.address, Addressselected));
            postData.Add(new KeyValuePair<string, string>(Constants.notes, getnotes));
            postData.Add(new KeyValuePair<string, string>(Constants.latitude, lat));
            postData.Add(new KeyValuePair<string, string>(Constants.longitude, longitude));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.Check_in, content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");
            Helpers.Settings.Displaybuttonstatus = true;
            Notes.Text = null;
            Address.SelectedIndex = 0;
            //await DisplayAlert("Success!", Helpers.Settings.Displaybuttonstatus.ToString(), "ok");
            btn_check.Text = "Check-out";
        }
        }
        private async void geteventid()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_event_id, content);

            var JsonResult = response.Content.ReadAsStringAsync().Result;
            result = JsonResult.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<Eventid>(result.ToString());
            eventid = rootobject.event_id;
           // await DisplayAlert("ok", eventid.ToString(), "ok");
            checkout(eventid, latitude, longitude);
        }
        private async void checkout(string eve, string lat, string longitude)
        {
            if (Addressselected.Equals("Choose Address"))
            {
               await DisplayAlert("OhSnap!", "Please select valid address", "ok");
            }
            else { 
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
                if (string.IsNullOrEmpty(setempid))
                {
                    setempid = Helpers.Settings.Displayemployeeid;
                }
                if (string.IsNullOrEmpty(setcompid))
                {
                    setcompid = Helpers.Settings.Displaycompanyid;
                }
                // await DisplayAlert("check out  companyid", setcompid, "okay");
                postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            postData.Add(new KeyValuePair<string, string>(Constants.address, Addressselected));
            postData.Add(new KeyValuePair<string, string>(Constants.notes, getnotes));
            postData.Add(new KeyValuePair<string, string>(Constants.latitude, lat));
            postData.Add(new KeyValuePair<string, string>(Constants.longitude, longitude));
            postData.Add(new KeyValuePair<string, string>("parent_id", eve));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.Check_out, content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");
            Helpers.Settings.Displaybuttonstatus = false;
            Notes.Text = null;
            // Address.SelectedIndex = -1;
            Address.SelectedIndex = 0;
            btn_check.Text = "Check-in";
        }
        }
        static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
        private void ok_clicked(object sender, EventArgs e)
        {
            btn_addedit.IsEnabled = false;
            var get_string = EnteredName.Text;
            if (string.IsNullOrEmpty(get_string))
            {

            
          
                DisplayAlert("OhSnap!", "Enter some valid address", "ok");
            }
            else
            {



                get_string = UppercaseFirst(get_string);
                get_string = get_string.Substring(0, 1) + get_string.Substring(1).ToLower();

                if (btn_addedit.Text == "Add")
                {
                    Add_address(get_string);
                    Loadaddress();

                }
                if (btn_addedit.Text == "Edit")
                {
                    Edit_Address(get_string);

                    Loadaddress();
                }
            }
        }

        private async void Edit_Address(string get_string)
        {

            if (get_string.Equals("Choose Address"))
            {
                await DisplayAlert("OhSnap!", "Please edit your given address Properly", "ok");
            }
            else
            {
                var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("id", get_id));
            postData.Add(new KeyValuePair<string, string>("address", get_string));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.Edit_address, content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
           
                if (JsonResult != null)
                {
                    await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");
                    overlay_checkin.IsVisible = false;
                    EnteredName.Text = null;
                    Loadaddress();
                }
                else
                {
                    await DisplayAlert("OhSnap!", "Please Edit the address Properly", "ok");

                }


                btn_addedit.IsEnabled = true;
            }



        }
        private async void Add_address(string get_string)
        {




            if (get_string.Equals("Choose Address"))
            {
                await DisplayAlert("OhSnap!", "Please edit your given address Properly", "ok");
            }
            else
            {

            
            //  Loadaddress();
            //   await  DisplayAlert("Address", get_string, "ok");
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();

          //  await DisplayAlert("add address  companyid", setcompid, "okay");
            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
            postData.Add(new KeyValuePair<string, string>(Constants.address, get_string));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.Add_address, content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;

            if (JsonResult != null)
            {
                await DisplayAlert("Success", JsonResult, "ok");
                overlay_checkin.IsVisible = false;
                EnteredName.Text = null;
                Loadaddress();

            }
            else
            {
                await DisplayAlert("OhSnap!", " Address Already Exists", "ok");

            }


            btn_addedit.IsEnabled = true;
        }

        }
        private void into_clicked(object sender, EventArgs e)
        {
            overlay_checkin.IsVisible = false;
        }
       public async void getlatlong()
        {
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;
            var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(1000));            
            string.Format("Time: {0} \nLat: {1} \nLong: {2} \n Altitude: {3} \nAltitude Accuracy: {4} \nAccuracy: {5} \n Heading: {6} \n Speed: {7}",
              position.Timestamp, position.Latitude, position.Longitude,
              position.Altitude, position.AltitudeAccuracy, position.Accuracy, position.Heading, position.Speed);
            latitude = position.Latitude.ToString();
            longitude = position.Longitude.ToString();
            if (locator.IsGeolocationEnabled)
            {
                Java.Lang.Thread.Sleep(2000);
                btn_check.IsEnabled = true;
            }
            else
            {

            }
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("latitude", latitude));
            postData.Add(new KeyValuePair<string, string>("longitude", longitude));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.get_latlong_address, content);
            var JsonResult = response.Content.ReadAsStringAsync().Result;
            string result = JsonResult.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<ItemAddress>(result.ToString());
            string loadadd = rootobject.area_name;
            string[] addresses = loadadd.Split(',');
            for (int i = 0; i < addresses.Length; i++)
            {
                a1 = addresses[0];
                b1 = addresses[1];
            }
            add_get = a1 + b1;            
            var action = await DisplayAlert("Would you like to add this location?", "Your current location is" + add_get, "Yes", "NO");
            if (action == false)
            {
                return;
            }
            else
            {               
                Address.Items.Add(add_get);                
            }            
        }
        public async void Loadaddress()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.JsonFormat));
            var postData = new List<KeyValuePair<string, string>>();
            if (string.IsNullOrEmpty(setempid))
            {
                setempid = Helpers.Settings.Displayemployeeid;
            }
            else
            {

            }
            if (string.IsNullOrEmpty(setcompid))
            {
                setcompid = Helpers.Settings.Displaycompanyid;
            }
            else
            {

            }
            postData.Add(new KeyValuePair<string, string>(Constants.emp_id, setempid));
            postData.Add(new KeyValuePair<string, string>(Constants.company_id, setcompid));
            var content = new FormUrlEncodedContent(postData);
            var response = await client.PostAsync(Constants.load_user_added_address, content);           
            string Jsonvalue = response.Content.ReadAsStringAsync().Result;
            if(Jsonvalue== "No records Found")
            {

                await DisplayAlert("OhSnap!","No address, you added before. try again", "ok");
            }

            else
            {
                string result = Jsonvalue.Trim(']').Trim('[');
                //await DisplayAlert("jsonResult", result, "ok");
                tokens = result.Split(',');
                for (int i = 0; i < tokens.Length; i++)
                {
                    string a = tokens[i];
                    string b = a.Trim('"').Trim('"');
                    string[] id = b.Split('-');
                     empname = id[1];
                    Address.Items.Remove(empname);
                    Address.Items.Remove(Addressselected);
                    Address.Items.Add(empname);
                   
                }                        
            }
        }
        public void check_connection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            string networkStatus = networkConnection.IsConnected ? "Connected" : "Not Connected";
            if (networkStatus.Equals("Not Connected"))
            {
                DisplayAlert("Whoops!", "No internet! Check your connection", "ok");
            }
            else
            {

            }
        }



        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = DisplayAlert("Alert!", "Do you want to submit the injury report before exit?", "Yes", "No");
                if (await result)
                {
                    await Navigation.PushAsync(new Report_injury());
                }
                else
                {
                    if (Device.OS == TargetPlatform.Android)

                        DependencyService.Get<IAndroidMethods>().CloseApp();

                    // base.OnBackButtonPressed();
                }

            });

            return true;
        }
        internal class Eventid
        {
            public string event_id { get; set; }
            public Eventid(string event_id)
            {
                this.event_id = event_id;
            }
        }
    }
}