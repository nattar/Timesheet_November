﻿
using Newtonsoft.Json;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeSheet_Inhouse.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class check : ContentPage, IElementConfiguration<Frame>
    {
        Label label, latittude, lathead, longhead, Longititude, timehead, time, addhead, noteshead, Datehead, date;
        string setempid, eventid, JsonResult, result, latitude, longitude, getaddress, getnotes, eve_id;
        Entry Address, Notes;
        Button btn_check;
        Boolean swtich_status;
        Switch switcher;
        // Normal button height
        private const int BUTTON_HEIGHT = 66;
        private const int BUTTON_HEIGHT_WP = 144;
        private const int BUTTON_HALF_HEIGHT = 44;
        private const int BUTTON_HALF_HEIGHT_WP = 72;
        private const int BUTTON_WIDTH = 120;
        private const int BUTTON_WIDTH_WP = 144;
        private const int BUTTON_BORDER_WIDTH = 50;
        public check()
        {
            InitializeComponent();
            Title = "Checkin/out";
            if (App.Current.Properties.ContainsKey("employeeid"))
                setempid = (string)App.Current.Properties["employeeid"];
            //Boolean getval = Helpers.Settings.DisplaySwitchstatus;
           // DisplayAlert("ok", getval.ToString(), "ok");
            label = new Label
            {
                Text = "Check-in/out"
            };
            switcher = new Switch
            {
                WidthRequest = 200,
                HeightRequest = 25
            };
            //if (getval == true)
            //{
            //    switcher.IsToggled = true;
            //}
            lathead = new Label
            {
                Text = "latitude       ",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
            };

            latittude = new Label
            {
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
            };
            Longititude = new Label
            {
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
            };

            longhead = new Label
            {
                Text = "Longititude",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
            };
            timehead = new Label
            {
                Text = "Date             ",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
            };
            time = new Label
            {
                Text = DateTime.Now.ToString("MM-dd-yyyy"),
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
            };

            addhead = new Label
            {
                Text = "Address       ",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
            };
            noteshead = new Label
            {
                Text = "Notes         ",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
            };
            Datehead = new Label
            {
                Text = "Time             ",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
            };
            date = new Label
            {
                Text = DateTime.Now.ToString("HH:mm tt"),
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                //DateTime time = DateTime.Today + Time;

                //FormattedTime.Text = string.Format("{0: hh:mm tt}", time);

            };
            Address = new Entry
            {

                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Entry)),
            };
            Notes = new Entry
            {
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Entry)),
            };
            btn_check = new Button
            {
                BackgroundColor = Color.FromHex("#0077b9"),
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Center,
                BorderWidth = BUTTON_BORDER_WIDTH,
                BorderRadius = Device.OnPlatform(BUTTON_HALF_HEIGHT, BUTTON_HALF_HEIGHT, BUTTON_HALF_HEIGHT_WP),
                HeightRequest = Device.OnPlatform(BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT_WP),
                MinimumHeightRequest = Device.OnPlatform(BUTTON_HEIGHT, BUTTON_HEIGHT, BUTTON_HEIGHT_WP),
                WidthRequest = Device.OnPlatform(BUTTON_WIDTH, BUTTON_WIDTH, BUTTON_WIDTH_WP),
                MinimumWidthRequest = Device.OnPlatform(BUTTON_WIDTH, BUTTON_WIDTH, BUTTON_WIDTH_WP),

                //BorderRadius = 20,
            };
            btn_check.IsVisible = false;
            btn_check.IsEnabled = false;
              switcher.Toggled += switcher_Toggled;
            btn_check.Clicked += btn_check_send;

            this.Content = new StackLayout
            {
                BackgroundColor = Color.FromHex("E0E0E0"),
                Padding = new Thickness(10, 5, 0, 0),
                Spacing = 8,
                Children =
                {
                    new StackLayout
                    {
                        Padding=new Thickness(100,10,0,0),

                        Children=
                        {
                            switcher
                        }
                    },
                    new StackLayout
                    {
                          Padding=new Thickness(20,3,20,0),
                          Children=
                        {
                            new Frame
                            {
                    BackgroundColor=Color.FromHex("FFFFFF"),
                    OutlineColor=Color.White,
                    HasShadow=true,
                    Content=new StackLayout
                    {
                         Padding = new Thickness(10,0,0,0),
                         Spacing=3,
                         Children=
                        {
                            new StackLayout
                            {
                            Orientation=StackOrientation.Horizontal,
                            Children=
                                {
                                       lathead,latittude
                                }
                            },
                            new StackLayout
                            {
                            Orientation=StackOrientation.Horizontal,
                            Children=
                                {
                                       longhead,Longititude
                                }
                            },
                            new StackLayout
                            {
                            Orientation=StackOrientation.Horizontal,
                            Children=
                                {
                                       timehead,time
                                }
                            },
                            new StackLayout
                            {
                            Orientation=StackOrientation.Horizontal,
                            Children=
                                {
                                       Datehead,date                                }
                            },
                            new StackLayout
                            {
                            Orientation=StackOrientation.Vertical,
                           Children=
                                {
                                      addhead,Address
                                }
                            },
                            new StackLayout
                            {
                            Orientation=StackOrientation.Vertical,
                            Children=
                                {
                                      noteshead,Notes
                                }
                            },btn_check

                        }
                    }


                            }
                        }
                    }

                }
            };

        }

        private void btn_check_send(object sender, EventArgs e)
        {
            getaddress = Address.Text;
            string btn_value = btn_check.Text.ToString();
            DisplayAlert("ok", btn_value, "ok");
            if (btn_value.Equals("Check-in"))
            {
                checkin(latitude, longitude);
            }

            else if (btn_value.Equals("Check-out"))
            {
                geteventid();

                // checkout(latitude, longitude);
            }
        }

        private async void geteventid()
        {
            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("user_id", setempid));


            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync("http://vps4.techiva.com/projects/Ts_sample/public/api/get_checkedin_user", content);

            var JsonResult = response.Content.ReadAsStringAsync().Result;
            result = JsonResult.Trim(']').Trim('[');
            var rootobject = JsonConvert.DeserializeObject<Eventid>(result.ToString());
            eventid = rootobject.event_id;
            checkout(eventid, latitude, longitude);
        }

        private async void checkout(string eventid, string latitude, string longitude)
        {
            await DisplayAlert("Success!", "parent id " + eventid, "ok");
            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("user_id", setempid));
            postData.Add(new KeyValuePair<string, string>("address", getaddress));
            // postData.Add(new KeyValuePair<string, string>("notes", getnotes));
            postData.Add(new KeyValuePair<string, string>("latitude", latitude));
            postData.Add(new KeyValuePair<string, string>("longitude", longitude));
            postData.Add(new KeyValuePair<string, string>("parent_id", eventid));

            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync("http://vps4.techiva.com/projects/Ts_sample/public/api/check_out", content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");

        }

        private async void checkin(string latitude, string longitude)
        {
            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("user_id", setempid));
            postData.Add(new KeyValuePair<string, string>("address", getaddress));
            // postData.Add(new KeyValuePair<string, string>("notes", getnotes));
            postData.Add(new KeyValuePair<string, string>("latitude", latitude));
            postData.Add(new KeyValuePair<string, string>("longitude", longitude));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync("http://vps4.techiva.com/projects/Ts_sample/public/api/check_in", content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");
            //TODO: TEST CODE
            #region LOCATION PART

            //TODO: FAKE:
            MessagingCenter.Send(new Message(true), string.Empty);
            // return;

            #endregion
        }

        private async void switcher_Toggled(object sender, ToggledEventArgs e)
        {
            swtich_status = e.Value;
           //Helpers.Settings.DisplaySwitchstatus = e.Value;
            if (swtich_status == true)
            {
                btn_check.IsVisible = true;
                btn_check.Text = "Check-in";
                // checkin(latitude, longitude);
            }

            else if (swtich_status == false)
            {
                btn_check.IsVisible = true;
                btn_check.Text = "Check-out";
                //checkout(latitude, longitude);
            }
            else
            {

            }

            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 10;


            var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10000));

            if (position == null)
            {
                latittude.Text = "getting...";
                Longititude.Text = "getting...";

                return;
            }
            string.Format("Time: {0} \nLat: {1} \nLong: {2} \n Altitude: {3} \nAltitude Accuracy: {4} \nAccuracy: {5} \n Heading: {6} \n Speed: {7}",
              position.Timestamp, position.Latitude, position.Longitude,
              position.Altitude, position.AltitudeAccuracy, position.Accuracy, position.Heading, position.Speed);
            latittude.Text = position.Latitude.ToString();
            Longititude.Text = position.Longitude.ToString();
            latitude = position.Latitude.ToString();
            longitude = position.Longitude.ToString();
            btn_check.IsEnabled = true;

        }
        //TEST CODE
        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<Position>(this, string.Empty, OnLocationUpdate); //One
        }

        private async void OnLocationUpdate(Position position)
        {
            //Send to server
            System.Diagnostics.Debug.WriteLine(nameof(OnLocationUpdate) + " Location " + position.Latitude + " " + position.Longitude);
            string lat = position.Latitude.ToString();
            string longitttitude = position.Longitude.ToString();
            Java.Lang.Thread.Sleep(2000);
            var client = new System.Net.Http.HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("user_id", setempid));
            postData.Add(new KeyValuePair<string, string>("event_id","1"));
            // postData.Add(new KeyValuePair<string, string>("notes", getnotes));
            postData.Add(new KeyValuePair<string, string>("latitude",lat));
            postData.Add(new KeyValuePair<string, string>("longitude", longitttitude));
            var content = new System.Net.Http.FormUrlEncodedContent(postData);
            var response = await client.PostAsync("http://vps4.techiva.com/projects/Ts_sample/public/api/getting_location", content);
            //to take the value direstly without the array 
            JsonResult = response.Content.ReadAsStringAsync().Result;
            await DisplayAlert("Success!", "Thank you!! " + JsonResult, "ok");


        }

        IPlatformElementConfiguration<T, Frame> IElementConfiguration<Frame>.On<T>()
        {
            throw new NotImplementedException();
        }
    }
}