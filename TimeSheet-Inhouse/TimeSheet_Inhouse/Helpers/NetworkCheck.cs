﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse.Helpers
{
    class NetworkCheck
    {
        public static bool IsNetworkConnected()
        {
            bool retVal = false;

            try
            {
                retVal = CrossConnectivity.Current.IsConnected;
                return retVal;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
            }
        }
    }
}
