// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace TimeSheet_Inhouse.Helpers
{
  /// <summary>
  /// This is the Settings static class that can be used in your Core solution or in any
  /// of your client applications. All settings are laid out the same exact way with getters
  /// and setters. 
  /// </summary>
  public static class Settings
  {

        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = "hello";

        #endregion



        //-----------SWITCHSTATUS
        #region Setting Constants
        // Displayswitch status
        private const string DisplayswitchstatusKey = "display_switch_status_key";
        private static readonly bool DisplayswitchstatusDefault = false;
        #endregion

        public static bool DisplaySwitchstatus
        {
            get
            {
                return AppSettings.GetValueOrDefault(DisplayswitchstatusKey, DisplayswitchstatusDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(DisplayswitchstatusKey, value);
            }
        }


        //----------------------loginemail account active check
        #region Setting Constants
        // Display loginemail status
        private const string loginactiveemailstatus_key = "Loginactiveemailstatus_key";

        private static readonly string loginactiveemailstatusdefault = null;
        #endregion

        public static string DisplayActiveEmailLoginstatus
        {
            get
            {
                return AppSettings.GetValueOrDefault(loginactiveemailstatus_key, loginactiveemailstatusdefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(loginactiveemailstatus_key, value);
            }
        }



        //----------------------loginemail account active check
        #region Setting Constants
        // Display loginemail status
        private const string activeemailstatus_key = "activeemailstatus_key";

        private static readonly string activeemailstatusdefault = null;
        #endregion

        public static string DisplayActiveEmailstatus
        {
            get
            {
                return AppSettings.GetValueOrDefault(activeemailstatus_key, activeemailstatusdefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(activeemailstatus_key, value);
            }
        }




















        //----------------------loginstatus
        #region Setting Constants
        // Displayswitch status
        private const string Loginstatus = "Loginstatus_key";
     
        private static readonly bool loginstatusdefault = false;
        #endregion

        public static bool DisplayLoginstatus
        {
            get
            {
                return AppSettings.GetValueOrDefault(Loginstatus, loginstatusdefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Loginstatus, value);
            }
        }


        //-----emprole from get role
        #region Setting Constants
        // Display emprole status from get role
        private const string accesscontrolkey = "accesscontrol2_key";

        private static readonly string accesscontroldefault = null;
        #endregion

        public static string DisplayAccesscontrolstatus
        {
            get
            {
                return AppSettings.GetValueOrDefault(accesscontrolkey, accesscontroldefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(accesscontrolkey, value);
            }
        }








        // Display controlhome - emprole from home status from get role

        #region Setting Constants

        private const string controlhomekey = "accesscontrol2_key";

        private static readonly string controlhomedefault = null;
        #endregion
        public static string Displaycontrolrole
        {
            get
            {
                return AppSettings.GetValueOrDefault(controlhomekey, controlhomedefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(controlhomekey, value);
            }
        }




        //// Display Companyname from home status from get role

        #region Setting Constants

        private const string companynamekey = "accesscontrol2_key";

        private static readonly string companynamedefault = null;
        #endregion
        public static string DisplayCompanynameset
        {
            get
            {
                return AppSettings.GetValueOrDefault(companynamekey, companynamedefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(companynamekey, value);
            }
        }



        #region Setting Constants
        // Display emp_name status from get role
        private const string emplnamekey = "emplnamekey";

        private static readonly string emplnamedefault = null;
        #endregion

        public static string DisplayEmployeename
        {
            get
            {
                return AppSettings.GetValueOrDefault(emplnamekey, emplnamedefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(emplnamekey, value);
            }
        }


//----------------------------compNY ID
        #region Setting Constants
        // Display companyid status from get role
        private const string companyidkey = "companyidkey";

        private static readonly string companyiddefault = null;
        #endregion

        public static string Displaycompanyid
        {
            get
            {
                return AppSettings.GetValueOrDefault(companyidkey, companyiddefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(companyidkey, value);
            }
        }




//-------------------EMPLOYEE ID


        #region Setting Constants
        // Display employeeid status from get role
        private const string emplidkey = "emplidkey";

        private static readonly string empliddefault = null;
        #endregion

        public static string Displayemployeeid
        {
            get
            {
                return AppSettings.GetValueOrDefault(emplidkey, empliddefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(emplidkey, value);
            }
        }

        //-------------------EMPLOY EMAIL
        #region Setting Constants
        // Display email status from get role
        private const string emailkey = "emailkey";

        private static readonly string emaildefault = null;
        #endregion

        public static string Displayemail
        {
            get
            {
                return AppSettings.GetValueOrDefault(emailkey, emaildefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(emailkey, value);
            }
        }







        //----------------------Timer Value
        #region Setting Constants
        // Display Timer status
        private const string timer_key = "Timer_key";

        private static readonly bool timerstatusdefault = false;

        #endregion

        public static bool DisplayTimerstatus
        {
            get
            {
                return AppSettings.GetValueOrDefault(timer_key, timerstatusdefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(timer_key, value);
            }
        }









        //-----------------buttonstatus
        #region Setting Constants
        // Displaybutton status
        private const string DisplaybuttonstatusKey = "display_button_status_key";
        private static readonly bool DisplaybuttonstatusDefault = false;
        #endregion

        public static bool Displaybuttonstatus
        {
            get
            {
                return AppSettings.GetValueOrDefault(DisplaybuttonstatusKey, DisplaybuttonstatusDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(DisplaybuttonstatusKey, value);
            }
        }


        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(SettingsKey, value);
            }
        }

    }
}