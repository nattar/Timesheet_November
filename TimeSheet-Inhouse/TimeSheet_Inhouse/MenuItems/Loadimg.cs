﻿namespace TimeSheet_Inhouse.MenuItems
{
    internal class Loadimg
    {
        public Loadimg(string company_name, string logo,string date_formats, string name)
        {
            this.company_name = company_name;
            this.logo = logo;
            this.date_formats = date_formats;
            this.name = name;
        }
        public string company_name { get; set; }
        public string logo { get; set; }
        public string date_formats { get; set;
        }
        public string time_track { get; set; }
        public string name { get; set; }
    }
}