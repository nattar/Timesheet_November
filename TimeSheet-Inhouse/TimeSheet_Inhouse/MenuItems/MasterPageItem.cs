﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace TimeSheet_Inhouse.MenuItems
{
    public class MasterPageItem 
    {
        public string Title { get; set; }
        public string Icon { get; set; }
        public Type TargetType { get; set; }

      
       
    }
}
