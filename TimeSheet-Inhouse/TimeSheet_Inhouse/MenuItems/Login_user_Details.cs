﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSheet_Inhouse.MenuItems
{
    class Login_user_Details
    {
        public string name { set; get; }
        public string emp_id { set; get; }
        public string permission { set; get; }
        //public string role
        //{
        //    set;get;
        //}
        public string company_name { get; set; }
        public string company_id
        {
            get; set;
        }
        public string email
        {
            get;set;
        }
        public string logo { get; set; }
        public string date_formats
        {
            get; set;
        }
        public string time_track { get; set; }
       
    }
}
