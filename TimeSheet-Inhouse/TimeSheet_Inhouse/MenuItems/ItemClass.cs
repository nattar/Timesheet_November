﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TimeSheet_Inhouse.MenuItems
{
   public class ItemClass: INotifyPropertyChanged
    {
        string dateformat;
       public ItemClass()
        {
            if (App.Current.Properties.ContainsKey("Dateformat"))
                dateformat = (string)App.Current.Properties["Dateformat"];
        }
        public Color _color;
        public Color itemcolor
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
               onPropertyChanged("itemcolor");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void onPropertyChanged(string v)
        {
            if (PropertyChanged == null)
                return;
        }
        public string project_name { get; set; }
        public string task_name { get; set; }
        public int hours { get; set; }
        public string Hourstring
        {
            get
            {
                TimeSpan time = TimeSpan.FromSeconds(hours);
                string str = time.ToString(@"hh\:mm");

                return str;
            }
        }
        //public string image { get; set; }
        public string approve_status { get; set; }
        public DateTime date { get; set;  }
        public string Datestring
        {           
            get
            {
                
                return date.ToString(dateformat);
            }
        }
        public DateTime start_date
        {
            get;
            set;
        }
        public string startdate
        {
            get
            {
                return start_date.ToString(dateformat);
            }
        }
        public string id { get; set; }
        //private bool _isRefreshing = false;
        //public bool IsRefreshing
        //{
        //    get { return _isRefreshing; }
        //    set
        //    {
        //        _isRefreshing = value;
        //        OnPropertyChanged(nameof(IsRefreshing));
        //    }
        //}
        //public ICommand RefreshCommand
        //{
        //    get
        //    {
        //        return new Command(async () =>
        //        {
        //            IsRefreshing = true;

        //            await LoadData();

        //            IsRefreshing = false;
        //        });
        //    }
        //}
    }
}
