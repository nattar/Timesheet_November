﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TimeSheet_Inhouse
{
    public class ShowHidePassEffect : RoutingEffect
    {
        public ShowHidePassEffect(string effectId) : base(effectId)
        {
        }
        public string EntryText { get; set; }
        public ShowHidePassEffect() : base("Xamarin.ShowHidePassEffect") { }
    }
}
